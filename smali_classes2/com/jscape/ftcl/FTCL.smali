.class public Lcom/jscape/ftcl/FTCL;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final interpretationService:Lcom/jscape/ftcl/b/f;

.field private final transferParameters:Lcom/jscape/ftcl/b/d;

.field private final variables:Lcom/jscape/ftcl/b/a/bw;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "\u0004\u0008uf\\\u0001:#\u000fje\u0019\'h\u0001\u000etn\u001d;,b-pm\u0019_B\u0017\u0012xd\u0019u\r\u001a$##\u001a!+.A4e\\&+0\u0008iw\u001a<$\'AB.\u0013%<+\u000ewp!_\u001d1\u0000~f\\\u001f\t\u0010[9i\u001d#)bLsb\u000eu;$\u0015i-\u00164:bL\u007f#\u000f6:+\u0011me\u00159-b:4l\u000c!!-\u000fj^v\u0007-#\u00059E(\u0016\u0004b\u0002vn\u00114&&\u00129e\u000e:%b\u00009p\u001f\'!2\u00159e\u00159-b\u0000wg\\00\'\u0002lw\u0019{BH.iw\u0015:&1[\u0013.\u0014\\<*\u00049p\u0019\'>\'\u00139k\u0013&<,\u0000tfvx2K\u0015qf\\&-0\u0017|q\\\u0001\u000b\u0012Ail\u000e!Bo\u0015\u0010w\u00140h1\u0004ku\u0019\'h!\u000ewm\u00196<+\u000ew#\u0008<%\'\u000elw\\<&b\u0012|`\u0013;,1k4vu! \'Ajf\u000e#-0Alp\u0019\'&#\u000c|\tQ%A6\t|#\u000f0:4\u0004k#\u000c4;1\u0016vq\u0018u\u0007\u0010Aiq\u0015#)6\u00049h\u0019,h2\u0000jp\u000c=:#\u0012|\tu\" \'\u000f9s\u000e<>#\u0015|#\u001701b2_W,z\u001b\u0011)9b\t! \'\u000fmj\u001f4<+\u000ew#\u0015&h7\u0012|gR_e1h|m\u001d7$\'\u00126g\u0015&) \r|p\\&-!\u0014kf\\\u0006\u000e\u001616P/\u001dh!\u000ewm\u00196<+\u000ew/v\\>#\rpg\\#).\u0014|p\\4:\'Amq\t04$\u0000up\u0019_e)hlp\u0019u;2\u0004zj\u001a<-&Aiq\u0015#)6\u00049h\u0019,h$\u000ek#\t&-b\u0008w#/\u0013\u001c\u0012NJP4_A2\u0013pu\u001d!-b\n|z\\4=6\t|m\u0008<+#\u0015pl\u0012_e&h|m\u001d7$\'\u00126g\u0015&) \r|p\\:=6\u0011lw\\3\'0Ajf\u000f&!-\u000f5#\n4$+\u0005\u0013\n\n4$7\u0004j#\u001d\'-b\u0015kv\u0019).#\rjfvx$K\u0015qf\\9\'%A\u007fj\u00100h6\u000e9t\u000e<<\'Avv\u0008%=6Amlv_\r:\u0000ts\u00100rH\u0007m`\u0010ue$A\u007fw\u000c&+0\u0008iwR!06A4k\\3<2Otz\u0014:;6Ozl\u0011ue7Asp\u0011<<*A4s\\&-!\u0013|wv"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/ftcl/FTCL;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/4 v5, 0x3

    if-eqz v4, :cond_5

    const/4 v6, 0x1

    if-eq v4, v6, :cond_4

    const/4 v6, 0x2

    if-eq v4, v6, :cond_3

    if-eq v4, v5, :cond_2

    const/4 v6, 0x4

    if-eq v4, v6, :cond_6

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v5, 0x37

    goto :goto_1

    :cond_1
    const/16 v5, 0x2a

    goto :goto_1

    :cond_2
    const/16 v5, 0x7c

    goto :goto_1

    :cond_3
    const/16 v5, 0x66

    goto :goto_1

    :cond_4
    const/16 v5, 0x1e

    goto :goto_1

    :cond_5
    const/16 v5, 0x3d

    :cond_6
    :goto_1
    const/16 v4, 0x7f

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 27

    invoke-static {}, Lcom/jscape/ftcl/b/e;->a()Lcom/jscape/ftcl/b/f;

    move-result-object v0

    new-instance v15, Lcom/jscape/ftcl/b/d;

    move-object v1, v15

    sget-object v2, Lcom/jscape/filetransfer/Protocol;->FTP:Lcom/jscape/filetransfer/Protocol;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    move-object/from16 v26, v15

    move-object/from16 v15, v16

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-direct/range {v1 .. v25}, Lcom/jscape/ftcl/b/d;-><init>(Lcom/jscape/filetransfer/Protocol;Ljava/lang/String;Ljava/lang/Integer;Lcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/io/PrintStream;Ljava/io/File;Ljava/io/File;Ljava/lang/String;Ljava/lang/Boolean;Lcom/jscape/filetransfer/TransferMode;Ljava/lang/String;Lcom/jscape/filetransfer/AftpSecurityMode;Lcom/jscape/util/A;Lcom/jscape/util/A;Ljava/lang/Boolean;Ljava/lang/Boolean;[Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Boolean;)V

    new-instance v1, Lcom/jscape/ftcl/b/a/bw;

    invoke-direct {v1}, Lcom/jscape/ftcl/b/a/bw;-><init>()V

    move-object/from16 v2, p0

    move-object/from16 v3, v26

    invoke-direct {v2, v0, v3, v1}, Lcom/jscape/ftcl/FTCL;-><init>(Lcom/jscape/ftcl/b/f;Lcom/jscape/ftcl/b/d;Lcom/jscape/ftcl/b/a/bw;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/ftcl/b/f;Lcom/jscape/ftcl/b/d;Lcom/jscape/ftcl/b/a/bw;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/ftcl/FTCL;->interpretationService:Lcom/jscape/ftcl/b/f;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/ftcl/FTCL;->transferParameters:Lcom/jscape/ftcl/b/d;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/ftcl/FTCL;->variables:Lcom/jscape/ftcl/b/a/bw;

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private static close(Ljava/io/PrintStream;)V
    .locals 1

    invoke-static {}, Lcom/jscape/ftcl/c;->c()I

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    if-eq p0, v0, :cond_1

    :cond_0
    invoke-static {p0}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    :cond_1
    return-void
.end method

.method private static debugStreamFor(Ljava/lang/String;)Ljava/io/PrintStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/ftcl/c;->b()I

    move-result v0

    if-nez p0, :cond_0

    :try_start_0
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/ftcl/FTCL;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object p0

    if-nez v0, :cond_1

    :try_start_1
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_1

    :try_start_2
    invoke-static {p0}, Lcom/jscape/util/Q;->b(Ljava/io/File;)Ljava/io/File;

    goto :goto_0

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/ftcl/FTCL;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/ftcl/FTCL;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    new-instance p0, Ljava/io/PrintStream;

    new-instance v0, Ljava/io/FileOutputStream;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v2, v1}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;ZLjava/lang/String;)V

    return-object p0
.end method

.method public static execute(Lcom/jscape/ftcl/k;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/jscape/ftcl/b/e;->a()Lcom/jscape/ftcl/b/f;

    move-result-object v1

    invoke-virtual {p0}, Lcom/jscape/ftcl/k;->b()Lcom/jscape/ftcl/b/d;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/ftcl/k;->w:Ljava/lang/String;

    invoke-static {v3}, Lcom/jscape/ftcl/FTCL;->debugStreamFor(Ljava/lang/String;)Ljava/io/PrintStream;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/jscape/ftcl/b/d;->setDebugStream(Ljava/io/PrintStream;)V

    iget-object v3, p0, Lcom/jscape/ftcl/k;->x:Lcom/jscape/ftcl/b/a/bw;

    invoke-static {v2, v3}, Lcom/jscape/ftcl/b/a/b;->a(Lcom/jscape/ftcl/b/d;Lcom/jscape/ftcl/b/a/bw;)V

    new-instance v4, Lcom/jscape/ftcl/FTCL;

    invoke-direct {v4, v1, v2, v3}, Lcom/jscape/ftcl/FTCL;-><init>(Lcom/jscape/ftcl/b/f;Lcom/jscape/ftcl/b/d;Lcom/jscape/ftcl/b/a/bw;)V

    new-instance v1, Ljava/io/File;

    iget-object p0, p0, Lcom/jscape/ftcl/k;->n:Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Lcom/jscape/ftcl/FTCL;->execute(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0}, Lcom/jscape/ftcl/FTCL;->close(Ljava/io/PrintStream;)V

    return-void

    :catchall_0
    move-exception p0

    invoke-static {v0}, Lcom/jscape/ftcl/FTCL;->close(Ljava/io/PrintStream;)V

    throw p0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/jscape/util/a/j;->a()Lcom/jscape/util/a/j;

    move-result-object v0

    const/4 v1, 0x1

    :try_start_0
    invoke-static {p0}, Lcom/jscape/ftcl/k;->a([Ljava/lang/String;)Lcom/jscape/ftcl/k;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/ftcl/FTCL;->execute(Lcom/jscape/ftcl/k;)V

    const/4 p0, 0x0

    invoke-static {p0}, Ljava/lang/System;->exit(I)V
    :try_end_0
    .catch Lcom/jscape/util/a/a/f; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p0

    invoke-static {v0, p0}, Lcom/jscape/ftcl/FTCL;->showError(Lcom/jscape/util/a/j;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_0
    invoke-static {v0}, Lcom/jscape/ftcl/FTCL;->showUsage(Lcom/jscape/util/a/j;)V

    :goto_0
    invoke-static {v1}, Ljava/lang/System;->exit(I)V

    :goto_1
    return-void
.end method

.method private static showError(Lcom/jscape/util/a/j;Ljava/lang/Throwable;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/jscape/util/a/j;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method private static showUsage(Lcom/jscape/util/a/j;)V
    .locals 1

    sget-object v0, Lcom/jscape/ftcl/FTCL;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/jscape/util/a/j;->b(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public execute(Ljava/io/File;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/FTCLException;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/FTCL;->execute(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    return-void

    :catchall_0
    move-exception p1

    move-object v0, v1

    goto :goto_1

    :catch_0
    move-exception p1

    move-object v0, v1

    goto :goto_0

    :catchall_1
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    :goto_0
    :try_start_2
    new-instance v1, Lcom/jscape/ftcl/FTCLException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/jscape/ftcl/FTCLException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_1
    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    throw p1
.end method

.method public execute(Ljava/io/InputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/FTCLException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/ftcl/FTCL;->interpretationService:Lcom/jscape/ftcl/b/f;

    iget-object v1, p0, Lcom/jscape/ftcl/FTCL;->transferParameters:Lcom/jscape/ftcl/b/d;

    iget-object v2, p0, Lcom/jscape/ftcl/FTCL;->variables:Lcom/jscape/ftcl/b/a/bw;

    invoke-interface {v0, p1, v1, v2}, Lcom/jscape/ftcl/b/f;->a(Ljava/io/InputStream;Lcom/jscape/ftcl/b/d;Lcom/jscape/ftcl/b/a/bw;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    new-instance v0, Lcom/jscape/ftcl/FTCLException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/ftcl/FTCLException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public execute([B)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/FTCLException;
        }
    .end annotation

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {p0, v0}, Lcom/jscape/ftcl/FTCL;->execute(Ljava/io/InputStream;)V

    return-void
.end method

.method public setDebugStream(Ljava/io/PrintStream;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/FTCL;->transferParameters:Lcom/jscape/ftcl/b/d;

    invoke-virtual {v0, p1}, Lcom/jscape/ftcl/b/d;->setDebugStream(Ljava/io/PrintStream;)V

    return-void
.end method
