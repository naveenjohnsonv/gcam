.class public Lcom/jscape/ftcl/a/D;
.super Lcom/jscape/ftcl/a/a;


# static fields
.field private static final e:Lcom/jscape/util/a/c;

.field private static final f:Ljava/lang/String;

.field private static final g:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "~2\u0012\u0002\u0003m>\u001e"

    const/16 v5, 0x8

    move v7, v0

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/4 v9, 0x6

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x2

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x5d

    const/16 v4, 0x2e

    const-string v6, "\u0017rTInN,6xSY!F0<v\u0007lUpb ~U\\dRb2uC\ndX+\'h\u0007ZsO%!zJ\u0004.\u0017rTInN,6xSY!F0<v\u0007lUpb ~U\\dRb2uC\ndX+\'h\u0007ZsO%!zJ\u0004"

    move v7, v4

    move-object v4, v6

    move v8, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x5a

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/ftcl/a/D;->g:[Ljava/lang/String;

    aget-object v0, v1, v15

    sput-object v0, Lcom/jscape/ftcl/a/D;->f:Ljava/lang/String;

    new-instance v0, Lcom/jscape/util/a/c;

    new-array v1, v15, [Ljava/lang/String;

    sget-object v2, Lcom/jscape/ftcl/a/D;->g:[Ljava/lang/String;

    aget-object v4, v2, v3

    aput-object v4, v1, v3

    aget-object v2, v2, v10

    aput-object v2, v1, v10

    const v2, 0x7fffffff

    invoke-direct {v0, v1, v10, v2}, Lcom/jscape/util/a/c;-><init>([Ljava/lang/String;ZI)V

    sput-object v0, Lcom/jscape/ftcl/a/D;->e:Lcom/jscape/util/a/c;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    if-eq v2, v15, :cond_7

    const/4 v15, 0x3

    if-eq v2, v15, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v15, 0x5

    if-eq v2, v15, :cond_4

    const/16 v2, 0x18

    goto :goto_4

    :cond_4
    const/16 v2, 0x7a

    goto :goto_4

    :cond_5
    const/16 v2, 0x5b

    goto :goto_4

    :cond_6
    const/16 v2, 0x70

    goto :goto_4

    :cond_7
    const/16 v2, 0x7d

    goto :goto_4

    :cond_8
    const/16 v2, 0x41

    goto :goto_4

    :cond_9
    const/16 v2, 0x9

    :goto_4
    xor-int/2addr v2, v9

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransfer;)V
    .locals 3

    sget-object v0, Lcom/jscape/ftcl/a/D;->e:Lcom/jscape/util/a/c;

    invoke-virtual {v0}, Lcom/jscape/util/a/c;->h()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    sget-object v1, Lcom/jscape/ftcl/a/D;->g:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1, p1}, Lcom/jscape/ftcl/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/filetransfer/FileTransfer;)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/jscape/util/a/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object p1, p0, Lcom/jscape/ftcl/a/D;->a:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {p1}, Lcom/jscape/filetransfer/FileTransfer;->disconnect()V

    const/4 p1, 0x0

    invoke-static {p1}, Ljava/lang/System;->exit(I)V

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/jscape/ftcl/a/D;->e:Lcom/jscape/util/a/c;

    invoke-virtual {v0}, Lcom/jscape/util/a/c;->h()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method protected d()[Lcom/jscape/util/a/l;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/util/a/l;

    sget-object v1, Lcom/jscape/ftcl/a/D;->e:Lcom/jscape/util/a/c;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method
