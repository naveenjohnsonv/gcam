.class public Lcom/jscape/ftcl/a/s;
.super Lcom/jscape/ftcl/a/a;


# static fields
.field private static final e:Lcom/jscape/util/a/l;

.field private static final f:Lcom/jscape/util/a/l;

.field private static final g:Ljava/lang/String;

.field private static final h:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x2

    const/4 v4, 0x0

    const-string v5, "\u0010f\u0007\u0008{}\u001fyy\u0004\u001d6lv\u0010uq\u0012Dew\u0012yxA\u0002`t\u00147p\u0008\u0016l{\u0005wf\u0018J"

    const/16 v6, 0x28

    move v8, v3

    move v9, v4

    const/4 v7, -0x1

    :goto_0
    const/16 v10, 0x7d

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    move v15, v4

    :goto_2
    const/4 v2, 0x4

    const/4 v0, 0x3

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_1

    add-int/lit8 v0, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v0

    const/4 v0, 0x5

    goto :goto_0

    :cond_0
    const/16 v6, 0x22

    const/16 v2, 0x1d

    const-string v5, "\u007f%?Y<8[\r,>[01\u0008K)=]~9A_%2L>/Q\u0003\u0004K2>U"

    move v9, v0

    move v8, v2

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v8, v0

    move v9, v12

    :goto_3
    const/16 v10, 0x34

    add-int/2addr v7, v11

    add-int v0, v7, v8

    invoke-virtual {v5, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v13, v4

    const/4 v0, 0x5

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/ftcl/a/s;->h:[Ljava/lang/String;

    aget-object v0, v1, v0

    sput-object v0, Lcom/jscape/ftcl/a/s;->g:Ljava/lang/String;

    new-instance v0, Lcom/jscape/util/a/d;

    sget-object v1, Lcom/jscape/ftcl/a/s;->h:[Ljava/lang/String;

    aget-object v5, v1, v11

    aget-object v2, v1, v2

    invoke-direct {v0, v5, v2, v11, v4}, Lcom/jscape/util/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;ZI)V

    sput-object v0, Lcom/jscape/ftcl/a/s;->e:Lcom/jscape/util/a/l;

    new-instance v0, Lcom/jscape/util/a/e;

    aget-object v1, v1, v4

    invoke-direct {v0, v1, v3}, Lcom/jscape/util/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/jscape/ftcl/a/s;->f:Lcom/jscape/util/a/l;

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v4, v15, 0x7

    const/16 v17, 0x65

    if-eqz v4, :cond_8

    if-eq v4, v11, :cond_7

    if-eq v4, v3, :cond_6

    if-eq v4, v0, :cond_5

    if-eq v4, v2, :cond_6

    const/4 v0, 0x5

    if-eq v4, v0, :cond_4

    const/16 v17, 0x1c

    goto :goto_4

    :cond_4
    const/16 v17, 0x69

    goto :goto_4

    :cond_5
    const/4 v0, 0x5

    const/16 v17, 0xc

    goto :goto_4

    :cond_6
    const/4 v0, 0x5

    goto :goto_4

    :cond_7
    const/4 v0, 0x5

    const/16 v17, 0x74

    goto :goto_4

    :cond_8
    const/4 v0, 0x5

    const/16 v17, 0x19

    :goto_4
    xor-int v2, v10, v17

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v12, v15

    add-int/lit8 v15, v15, 0x1

    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransfer;)V
    .locals 3

    sget-object v0, Lcom/jscape/ftcl/a/s;->e:Lcom/jscape/util/a/l;

    invoke-interface {v0}, Lcom/jscape/util/a/l;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/ftcl/a/s;->h:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1, p1}, Lcom/jscape/ftcl/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/filetransfer/FileTransfer;)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/jscape/util/a/i;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/ftcl/a/s;->a:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->getLocalDir()Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/jscape/ftcl/a/s;->e:Lcom/jscape/util/a/l;

    invoke-virtual {p1, v2}, Lcom/jscape/util/a/i;->a(Lcom/jscape/util/a/l;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/jscape/ftcl/a/s;->f:Lcom/jscape/util/a/l;

    invoke-virtual {p1, v3}, Lcom/jscape/util/a/i;->a(Lcom/jscape/util/a/l;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    return-void
.end method

.method protected d()[Lcom/jscape/util/a/l;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/jscape/util/a/l;

    sget-object v1, Lcom/jscape/ftcl/a/s;->e:Lcom/jscape/util/a/l;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/ftcl/a/s;->f:Lcom/jscape/util/a/l;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    return-object v0
.end method
