.class public Lcom/jscape/ftcl/a/C;
.super Lcom/jscape/ftcl/a/a;


# static fields
.field private static final e:Lcom/jscape/util/a/l;

.field private static final f:Ljava/lang/String;

.field private static final g:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x28

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x1e

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\rq\u001f7M0Y>v\u0004+\\-\r}q\u00134V7\u001c}t\u0019+R*\u0017:#\u00120K&\u001a)l\u0004 \u0017(\rq\u001f7M0Y>v\u0004+\\-\r}q\u00134V7\u001c}t\u0019+R*\u0017:#\u00120K&\u001a)l\u0004 \u0017\u0003-t\u0012"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    const/4 v11, 0x2

    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x55

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/ftcl/a/C;->g:[Ljava/lang/String;

    aget-object v0, v1, v2

    sput-object v0, Lcom/jscape/ftcl/a/C;->f:Ljava/lang/String;

    new-instance v0, Lcom/jscape/util/a/f;

    sget-object v1, Lcom/jscape/ftcl/a/C;->g:[Ljava/lang/String;

    aget-object v1, v1, v11

    const v2, 0x7fffffff

    invoke-direct {v0, v1, v7, v2}, Lcom/jscape/util/a/f;-><init>(Ljava/lang/String;ZI)V

    sput-object v0, Lcom/jscape/ftcl/a/C;->e:Lcom/jscape/util/a/l;

    return-void

    :cond_1
    aget-char v12, v4, v10

    rem-int/lit8 v13, v10, 0x7

    if-eqz v13, :cond_7

    if-eq v13, v7, :cond_6

    if-eq v13, v11, :cond_5

    if-eq v13, v0, :cond_4

    const/4 v11, 0x4

    if-eq v13, v11, :cond_3

    const/4 v11, 0x5

    if-eq v13, v11, :cond_2

    const/16 v11, 0x67

    goto :goto_2

    :cond_2
    const/16 v11, 0x5d

    goto :goto_2

    :cond_3
    const/16 v11, 0x27

    goto :goto_2

    :cond_4
    const/16 v11, 0x47

    goto :goto_2

    :cond_5
    const/16 v11, 0x68

    goto :goto_2

    :cond_6
    const/16 v11, 0x1d

    goto :goto_2

    :cond_7
    const/16 v11, 0x43

    :goto_2
    xor-int/2addr v11, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransfer;)V
    .locals 3

    sget-object v0, Lcom/jscape/ftcl/a/C;->e:Lcom/jscape/util/a/l;

    invoke-interface {v0}, Lcom/jscape/util/a/l;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/ftcl/a/C;->g:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1, p1}, Lcom/jscape/ftcl/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/filetransfer/FileTransfer;)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/jscape/util/a/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object p1, p0, Lcom/jscape/ftcl/a/C;->a:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {p1}, Lcom/jscape/filetransfer/FileTransfer;->getDir()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/ftcl/a/C;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected d()[Lcom/jscape/util/a/l;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/util/a/l;

    sget-object v1, Lcom/jscape/ftcl/a/C;->e:Lcom/jscape/util/a/l;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method
