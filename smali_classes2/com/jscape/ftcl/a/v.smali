.class public Lcom/jscape/ftcl/a/v;
.super Lcom/jscape/ftcl/a/a;


# static fields
.field private static final e:Lcom/jscape/util/a/l;

.field private static final f:Ljava/lang/String;

.field private static final g:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ht*bS\u0016\u0016Zo15MY\u001eRz,vAB\u0013TreaI[\u001f\u001bz*g\u0000WZIy(zTSZ]u)p\u000e\u0007Vs!aI[\u001f"

    const/16 v5, 0x37

    const/16 v6, 0x2f

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x69

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x2

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x3b

    const/16 v4, 0xb

    const-string v6, "sC\u0012@nimgO\u0013J/RN\u0010Xi,,`U\u000b\u000fwc$h@\u0016L{x)nH_[sa%!@\u0010]:m`sC\u0012@ni`gO\u0013J4"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v17, v6

    move v6, v4

    move-object/from16 v4, v17

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x53

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/ftcl/a/v;->g:[Ljava/lang/String;

    aget-object v0, v1, v3

    sput-object v0, Lcom/jscape/ftcl/a/v;->f:Ljava/lang/String;

    new-instance v0, Lcom/jscape/util/a/d;

    sget-object v1, Lcom/jscape/ftcl/a/v;->g:[Ljava/lang/String;

    aget-object v2, v1, v10

    aget-object v1, v1, v15

    const v3, 0x7fffffff

    invoke-direct {v0, v2, v1, v10, v3}, Lcom/jscape/util/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;ZI)V

    sput-object v0, Lcom/jscape/ftcl/a/v;->e:Lcom/jscape/util/a/l;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    if-eq v2, v15, :cond_7

    const/4 v15, 0x3

    if-eq v2, v15, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v15, 0x5

    if-eq v2, v15, :cond_4

    const/16 v2, 0x13

    goto :goto_4

    :cond_4
    const/16 v2, 0x5f

    goto :goto_4

    :cond_5
    const/16 v2, 0x49

    goto :goto_4

    :cond_6
    const/16 v2, 0x7c

    goto :goto_4

    :cond_7
    const/16 v2, 0x2c

    goto :goto_4

    :cond_8
    const/16 v2, 0x75

    goto :goto_4

    :cond_9
    const/16 v2, 0x52

    :goto_4
    xor-int/2addr v2, v9

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransfer;)V
    .locals 3

    sget-object v0, Lcom/jscape/ftcl/a/v;->e:Lcom/jscape/util/a/l;

    invoke-interface {v0}, Lcom/jscape/util/a/l;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/ftcl/a/v;->g:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1, p1}, Lcom/jscape/ftcl/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/filetransfer/FileTransfer;)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/jscape/util/a/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sget-object v0, Lcom/jscape/ftcl/a/v;->e:Lcom/jscape/util/a/l;

    invoke-virtual {p1, v0}, Lcom/jscape/util/a/i;->a(Lcom/jscape/util/a/l;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/ftcl/a/v;->a:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0, p1}, Lcom/jscape/filetransfer/FileTransfer;->getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/ftcl/a/v;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected d()[Lcom/jscape/util/a/l;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/util/a/l;

    sget-object v1, Lcom/jscape/ftcl/a/v;->e:Lcom/jscape/util/a/l;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method
