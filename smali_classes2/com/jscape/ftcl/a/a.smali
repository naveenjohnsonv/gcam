.class public abstract Lcom/jscape/ftcl/a/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/ftcl/i;


# static fields
.field private static d:[I

.field private static final j:[Ljava/lang/String;


# instance fields
.field protected a:Lcom/jscape/filetransfer/FileTransfer;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x2

    new-array v3, v2, [I

    invoke-static {v3}, Lcom/jscape/ftcl/a/a;->b([I)V

    const-string v6, "\u001d\u0015R>1!# \u0004\tP88;g!\u0014V: tq1FW61h#&\tGy1cj;\u0012\ty"

    const/16 v7, 0x28

    const/4 v8, -0x1

    const/4 v9, 0x7

    const/4 v10, 0x0

    :goto_0
    const/16 v11, 0x10

    const/4 v12, 0x1

    add-int/2addr v8, v12

    add-int v13, v8, v9

    invoke-virtual {v6, v8, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    const/16 v14, 0xb

    const/4 v15, -0x1

    :goto_1
    invoke-virtual {v13}, Ljava/lang/String;->toCharArray()[C

    move-result-object v13

    array-length v3, v13

    const/4 v4, 0x0

    :goto_2
    const/16 v16, 0x23

    if-gt v3, v4, :cond_3

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v13}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v4, v10, 0x1

    if-eqz v15, :cond_1

    aput-object v3, v1, v10

    add-int/2addr v8, v9

    if-ge v8, v7, :cond_0

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v9

    move v10, v4

    goto :goto_0

    :cond_0
    const-string v6, "\u0016\u000fZ-=:8\u001b\u001f\u0012K## ~:\u0011Mb+o} ]F-; }+\u0014[6u "

    move v10, v4

    move/from16 v7, v16

    const/4 v8, -0x1

    const/4 v9, 0x7

    goto :goto_3

    :cond_1
    aput-object v3, v1, v10

    add-int/2addr v8, v9

    if-ge v8, v7, :cond_2

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v3

    move v9, v3

    move v10, v4

    :goto_3
    add-int/2addr v8, v12

    add-int v3, v8, v9

    invoke-virtual {v6, v8, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    move v11, v14

    const/4 v15, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/ftcl/a/a;->j:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v17, v13, v4

    rem-int/lit8 v5, v4, 0x7

    if-eqz v5, :cond_8

    if-eq v5, v12, :cond_7

    if-eq v5, v2, :cond_9

    const/4 v2, 0x3

    if-eq v5, v2, :cond_6

    if-eq v5, v0, :cond_5

    const/4 v2, 0x5

    if-eq v5, v2, :cond_4

    const/16 v16, 0x13

    goto :goto_4

    :cond_4
    move/from16 v16, v14

    goto :goto_4

    :cond_5
    const/16 v16, 0x44

    goto :goto_4

    :cond_6
    const/16 v16, 0x49

    goto :goto_4

    :cond_7
    const/16 v16, 0x76

    goto :goto_4

    :cond_8
    const/16 v16, 0x58

    :cond_9
    :goto_4
    xor-int v2, v11, v16

    xor-int v2, v17, v2

    int-to-char v2, v2

    aput-char v2, v13, v4

    add-int/lit8 v4, v4, 0x1

    const/4 v2, 0x2

    goto :goto_2
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/ftcl/a/a;->b:Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/jscape/ftcl/a/a;->c:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/filetransfer/FileTransfer;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/ftcl/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p3}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/ftcl/a/a;->a:Lcom/jscape/filetransfer/FileTransfer;

    return-void
.end method

.method private static a(Lcom/jscape/ftcl/c;)Lcom/jscape/ftcl/c;
    .locals 0

    return-object p0
.end method

.method public static b([I)V
    .locals 0

    sput-object p0, Lcom/jscape/ftcl/a/a;->d:[I

    return-void
.end method

.method public static e()[I
    .locals 1

    sget-object v0, Lcom/jscape/ftcl/a/a;->d:[I

    return-object v0
.end method


# virtual methods
.method protected a([Ljava/lang/String;Ljava/lang/String;)J
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/c;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0, p2}, Lcom/jscape/ftcl/a/a;->a([Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    int-to-long p1, p1

    return-wide p1

    :catch_0
    new-instance p1, Lcom/jscape/ftcl/c;

    invoke-direct {p1, p2}, Lcom/jscape/ftcl/c;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/a/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method protected a([Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/c;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/ftcl/a/a;->e()[I

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_3

    :cond_0
    :try_start_0
    array-length v1, p1
    :try_end_0
    .catch Lcom/jscape/ftcl/c; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    if-lt v1, p2, :cond_3

    :try_start_1
    aget-object v1, p1, p2
    :try_end_1
    .catch Lcom/jscape/ftcl/c; {:try_start_1 .. :try_end_1} :catch_2

    if-eqz v0, :cond_2

    :try_start_2
    invoke-static {v1}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v1
    :try_end_2
    .catch Lcom/jscape/ftcl/c; {:try_start_2 .. :try_end_2} :catch_3

    :cond_1
    if-eqz v1, :cond_3

    aget-object v1, p1, p2

    :cond_2
    return-object v1

    :cond_3
    :try_start_3
    new-instance p1, Lcom/jscape/ftcl/c;

    invoke-direct {p1, p3}, Lcom/jscape/ftcl/c;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_3
    .catch Lcom/jscape/ftcl/c; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/a/a;->a(Lcom/jscape/ftcl/c;)Lcom/jscape/ftcl/c;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/ftcl/a/a;->a(Lcom/jscape/ftcl/c;)Lcom/jscape/ftcl/c;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/ftcl/c; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/ftcl/a/a;->a(Lcom/jscape/ftcl/c;)Lcom/jscape/ftcl/c;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/ftcl/c; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/a/a;->a(Lcom/jscape/ftcl/c;)Lcom/jscape/ftcl/c;

    move-result-object p1

    throw p1
.end method

.method protected abstract a(Lcom/jscape/util/a/i;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method protected a(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method protected a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 p2, 0x1

    aput-object p3, v0, p2

    invoke-virtual {p0, p1, v0}, Lcom/jscape/ftcl/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    invoke-static {p1, p2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/ftcl/a/a;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a([Ljava/lang/String;)V
    .locals 3

    new-instance v0, Lcom/jscape/util/a/i;

    invoke-virtual {p0}, Lcom/jscape/ftcl/a/a;->d()[Lcom/jscape/util/a/l;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/util/a/i;-><init>([Lcom/jscape/util/a/l;)V

    :try_start_0
    invoke-virtual {v0, p1}, Lcom/jscape/util/a/i;->a([Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/ftcl/a/a;->a(Lcom/jscape/util/a/i;)V
    :try_end_0
    .catch Lcom/jscape/ftcl/c; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/ftcl/a/a;->j:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :catch_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/ftcl/a/a;->j:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :catch_1
    move-exception p1

    invoke-virtual {p1}, Lcom/jscape/ftcl/c;->getMessage()Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-virtual {p0, p1}, Lcom/jscape/ftcl/a/a;->a(Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method protected b([Ljava/lang/String;Ljava/lang/String;)J
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/c;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0, p2}, Lcom/jscape/ftcl/a/a;->a([Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide p1

    :catch_0
    new-instance p1, Lcom/jscape/ftcl/c;

    invoke-direct {p1, p2}, Lcom/jscape/ftcl/c;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected final b(Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/c;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/ftcl/a/a;->e()[I

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v2
    :try_end_0
    .catch Lcom/jscape/ftcl/c; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    if-eqz v2, :cond_0

    return-object v1

    :cond_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/jscape/ftcl/a/a;->a:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v2}, Lcom/jscape/filetransfer/FileTransfer;->getLocalDir()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    if-eqz v0, :cond_2

    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v2
    :try_end_1
    .catch Lcom/jscape/ftcl/c; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/a/a;->a(Lcom/jscape/ftcl/c;)Lcom/jscape/ftcl/c;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    if-eqz v2, :cond_3

    :cond_2
    return-object v1

    :cond_3
    new-instance v0, Lcom/jscape/ftcl/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/ftcl/a/a;->j:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/jscape/ftcl/c;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/ftcl/a/a;->a(Lcom/jscape/ftcl/c;)Lcom/jscape/ftcl/c;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/ftcl/c; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/a/a;->a(Lcom/jscape/ftcl/c;)Lcom/jscape/ftcl/c;

    move-result-object p1

    throw p1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final c(Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/c;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/ftcl/a/a;->e()[I

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2
    :try_end_0
    .catch Lcom/jscape/ftcl/c; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    if-eqz v2, :cond_0

    return-object v1

    :cond_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/jscape/ftcl/a/a;->a:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v2}, Lcom/jscape/filetransfer/FileTransfer;->getLocalDir()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    if-eqz v0, :cond_2

    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2
    :try_end_1
    .catch Lcom/jscape/ftcl/c; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/a/a;->a(Lcom/jscape/ftcl/c;)Lcom/jscape/ftcl/c;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    if-eqz v2, :cond_3

    :cond_2
    return-object v1

    :cond_3
    new-instance v0, Lcom/jscape/ftcl/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/ftcl/a/a;->j:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/jscape/ftcl/c;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/ftcl/a/a;->a(Lcom/jscape/ftcl/c;)Lcom/jscape/ftcl/c;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/ftcl/c; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/a/a;->a(Lcom/jscape/ftcl/c;)Lcom/jscape/ftcl/c;

    move-result-object p1

    throw p1
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/a/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method protected c([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/c;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/jscape/ftcl/a/a;->a([Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected abstract d()[Lcom/jscape/util/a/l;
.end method
