.class public Lcom/jscape/ftcl/a/g;
.super Lcom/jscape/ftcl/a/a;


# static fields
.field private static final e:Lcom/jscape/util/a/l;

.field private static final f:Lcom/jscape/util/a/l;

.field private static final g:Ljava/lang/String;

.field private static final h:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "\u0013\u0017\u0015-\u0016?u\u0016\u001e\u0002=\u001c2\u001c\u0016:\u001dwpX\u0016\u001e+\u0010po\u0012\u0001W<\u0014pv\u0010R\u001a7\u0015w-"

    const/16 v5, 0x28

    const/16 v6, 0xb

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x6c

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x3

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x22

    const/16 v4, 0x1c

    const-string v6, "\u0008&,\u0000\'MJb,$\u0011*JU(;m\u0006.JL*h \r/M\u0017\u0005)-/\u0017,"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v18, v6

    move v6, v4

    move-object/from16 v4, v18

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x56

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/ftcl/a/g;->h:[Ljava/lang/String;

    aget-object v0, v1, v10

    sput-object v0, Lcom/jscape/ftcl/a/g;->g:Ljava/lang/String;

    new-instance v0, Lcom/jscape/util/a/f;

    sget-object v1, Lcom/jscape/ftcl/a/g;->h:[Ljava/lang/String;

    aget-object v2, v1, v15

    invoke-direct {v0, v2, v10, v3}, Lcom/jscape/util/a/f;-><init>(Ljava/lang/String;ZI)V

    sput-object v0, Lcom/jscape/ftcl/a/g;->e:Lcom/jscape/util/a/l;

    new-instance v0, Lcom/jscape/util/a/e;

    aget-object v1, v1, v3

    invoke-direct {v0, v1, v3, v10}, Lcom/jscape/util/a/e;-><init>(Ljava/lang/String;ZI)V

    sput-object v0, Lcom/jscape/ftcl/a/g;->f:Lcom/jscape/util/a/l;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v2, v14, 0x7

    const/16 v17, 0x1b

    if-eqz v2, :cond_8

    if-eq v2, v10, :cond_7

    const/4 v3, 0x2

    if-eq v2, v3, :cond_8

    if-eq v2, v15, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v17, 0x6f

    goto :goto_4

    :cond_4
    const/16 v17, 0x7e

    goto :goto_4

    :cond_5
    const/16 v17, 0x1d

    goto :goto_4

    :cond_6
    const/16 v17, 0x34

    goto :goto_4

    :cond_7
    const/16 v17, 0x1e

    :cond_8
    :goto_4
    xor-int v2, v9, v17

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransfer;)V
    .locals 3

    sget-object v0, Lcom/jscape/ftcl/a/g;->e:Lcom/jscape/util/a/l;

    invoke-interface {v0}, Lcom/jscape/util/a/l;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/ftcl/a/g;->h:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1, p1}, Lcom/jscape/ftcl/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/filetransfer/FileTransfer;)V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method protected a(Lcom/jscape/util/a/i;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/ftcl/a/a;->e()[I

    move-result-object v0

    sget-object v1, Lcom/jscape/ftcl/a/g;->f:Lcom/jscape/util/a/l;

    invoke-virtual {p1, v1}, Lcom/jscape/util/a/i;->a(Lcom/jscape/util/a/l;)Ljava/lang/String;

    move-result-object p1

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/jscape/ftcl/a/g;->a:Lcom/jscape/filetransfer/FileTransfer;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-interface {v1, p1}, Lcom/jscape/filetransfer/FileTransfer;->setDebug(Z)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/a/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    if-nez v0, :cond_4

    :cond_1
    :try_start_1
    iget-object p1, p0, Lcom/jscape/ftcl/a/g;->a:Lcom/jscape/filetransfer/FileTransfer;

    iget-object v1, p0, Lcom/jscape/ftcl/a/g;->a:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->getDebug()Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v0, :cond_3

    if-nez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    move v0, v1

    :goto_2
    invoke-interface {p1, v0}, Lcom/jscape/filetransfer/FileTransfer;->setDebug(Z)Lcom/jscape/filetransfer/FileTransfer;

    goto :goto_3

    :catch_1
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/ftcl/a/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/a/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_4
    :goto_3
    return-void
.end method

.method protected d()[Lcom/jscape/util/a/l;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/jscape/util/a/l;

    sget-object v1, Lcom/jscape/ftcl/a/g;->e:Lcom/jscape/util/a/l;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/ftcl/a/g;->f:Lcom/jscape/util/a/l;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    return-object v0
.end method
