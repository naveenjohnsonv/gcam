.class public Lcom/jscape/ftcl/a/u;
.super Lcom/jscape/ftcl/a/a;


# static fields
.field private static final e:Lcom/jscape/util/a/l;

.field private static final f:Ljava/lang/String;

.field private static final g:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "]\u001a\u000c\u0004\"w\u0018\u001d\u0003#\u007fe]\u0012\u001d\u0015#ki\\\u0018\u001aPnltS\u0015\u0000\u001ed-fY\u0011\u001d\u0015q#"

    const/16 v5, 0x27

    move v7, v0

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v7

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v3

    :goto_2
    const/4 v14, 0x3

    if-gt v12, v13, :cond_3

    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v12}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v11, :cond_1

    add-int/lit8 v11, v8, 0x1

    aput-object v10, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x29

    const/16 v4, 0x22

    const-string v6, "w\u0018\u001d\u0003#\u007fe]\u0012\u001d\u0015#ki\\\u0018\u001aPnltS\u0015\u0000\u001ed-fY\u0011\u001d\u0015q#\u0006V\u0014\u0005\u0004f\u007f"

    move v7, v4

    move-object v4, v6

    move v8, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v10, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    add-int/2addr v6, v9

    add-int v10, v6, v7

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v3

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/ftcl/a/u;->g:[Ljava/lang/String;

    aget-object v0, v1, v9

    sput-object v0, Lcom/jscape/ftcl/a/u;->f:Ljava/lang/String;

    new-instance v0, Lcom/jscape/util/a/d;

    sget-object v1, Lcom/jscape/ftcl/a/u;->g:[Ljava/lang/String;

    aget-object v2, v1, v3

    aget-object v1, v1, v14

    const v3, 0x7fffffff

    invoke-direct {v0, v2, v1, v9, v3}, Lcom/jscape/util/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;ZI)V

    sput-object v0, Lcom/jscape/ftcl/a/u;->e:Lcom/jscape/util/a/l;

    return-void

    :cond_3
    aget-char v15, v10, v13

    rem-int/lit8 v2, v13, 0x7

    const/4 v3, 0x5

    if-eqz v2, :cond_9

    if-eq v2, v9, :cond_8

    const/4 v9, 0x2

    if-eq v2, v9, :cond_7

    if-eq v2, v14, :cond_6

    if-eq v2, v0, :cond_5

    if-eq v2, v3, :cond_4

    move v2, v3

    goto :goto_4

    :cond_4
    const/16 v2, 0x8

    goto :goto_4

    :cond_5
    const/4 v2, 0x6

    goto :goto_4

    :cond_6
    const/16 v2, 0x75

    goto :goto_4

    :cond_7
    const/16 v2, 0x6c

    goto :goto_4

    :cond_8
    const/16 v2, 0x78

    goto :goto_4

    :cond_9
    const/16 v2, 0x35

    :goto_4
    xor-int/2addr v2, v3

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v10, v13

    add-int/lit8 v13, v13, 0x1

    const/4 v3, 0x0

    const/4 v9, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransfer;)V
    .locals 3

    sget-object v0, Lcom/jscape/ftcl/a/u;->e:Lcom/jscape/util/a/l;

    invoke-interface {v0}, Lcom/jscape/util/a/l;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/ftcl/a/u;->g:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1, p1}, Lcom/jscape/ftcl/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/filetransfer/FileTransfer;)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/jscape/util/a/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sget-object v0, Lcom/jscape/ftcl/a/u;->e:Lcom/jscape/util/a/l;

    invoke-virtual {p1, v0}, Lcom/jscape/util/a/i;->a(Lcom/jscape/util/a/l;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/ftcl/a/u;->a:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0, p1}, Lcom/jscape/filetransfer/FileTransfer;->mdownload(Ljava/lang/String;)V

    return-void
.end method

.method protected d()[Lcom/jscape/util/a/l;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/util/a/l;

    sget-object v1, Lcom/jscape/ftcl/a/u;->e:Lcom/jscape/util/a/l;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method
