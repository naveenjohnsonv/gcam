.class public Lcom/jscape/ftcl/m;
.super Lcom/jscape/ftcl/l;


# static fields
.field private static final k:Ljava/lang/String;

.field private static final l:Ljava/lang/String;

.field private static final m:Ljava/lang/String;

.field protected static final n:Lcom/jscape/util/a/l;

.field private static final q:[Ljava/lang/String;


# instance fields
.field private o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "\u0010\roMr<@\r\u0008\u001c\u0018oMr<@\r\u0008\u0010\roMr<@\r&\u001a\u000fqO~<]\u0010\u000fq\u0001o&Y\u001cZ?\tr2Y\u0015\t|Ho\u007fUY\u0005gQw6J\u0010\u00146"

    const/16 v5, 0x41

    move v8, v3

    const/4 v6, -0x1

    const/16 v7, 0x8

    :goto_0
    const/16 v9, 0x58

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x5

    const/4 v1, 0x3

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v1, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v1

    goto :goto_0

    :cond_0
    const/16 v5, 0xb

    const-string v4, "\u001e\u001amOp>B\u000f\u0002V\u0016"

    move v8, v1

    const/4 v6, -0x1

    const/16 v7, 0x8

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v1

    move v8, v11

    :goto_3
    const/16 v9, 0x5a

    add-int/2addr v6, v10

    add-int v1, v6, v7

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/ftcl/m;->q:[Ljava/lang/String;

    aget-object v2, v0, v3

    sput-object v2, Lcom/jscape/ftcl/m;->k:Ljava/lang/String;

    aget-object v2, v0, v10

    sput-object v2, Lcom/jscape/ftcl/m;->l:Ljava/lang/String;

    aget-object v0, v0, v10

    sput-object v0, Lcom/jscape/ftcl/m;->m:Ljava/lang/String;

    new-instance v0, Lcom/jscape/util/a/d;

    sget-object v2, Lcom/jscape/ftcl/m;->q:[Ljava/lang/String;

    aget-object v4, v2, v15

    aget-object v1, v2, v1

    const v2, 0x7fffffff

    invoke-direct {v0, v4, v1, v3, v2}, Lcom/jscape/util/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;ZI)V

    sput-object v0, Lcom/jscape/ftcl/m;->n:Lcom/jscape/util/a/l;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    if-eq v2, v1, :cond_6

    const/4 v1, 0x4

    if-eq v2, v1, :cond_5

    if-eq v2, v15, :cond_4

    const/16 v1, 0x71

    goto :goto_4

    :cond_4
    const/4 v1, 0x7

    goto :goto_4

    :cond_5
    const/16 v1, 0x43

    goto :goto_4

    :cond_6
    const/16 v1, 0x79

    goto :goto_4

    :cond_7
    const/16 v1, 0x47

    goto :goto_4

    :cond_8
    const/16 v1, 0x38

    goto :goto_4

    :cond_9
    const/16 v1, 0x21

    :goto_4
    xor-int/2addr v1, v9

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method public constructor <init>([Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/c;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/ftcl/l;-><init>([Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/ftcl/m;->f:Lcom/jscape/util/a/i;

    invoke-direct {p0, p1}, Lcom/jscape/ftcl/m;->b(Lcom/jscape/util/a/i;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/ftcl/m;->o:Ljava/lang/String;

    return-void
.end method

.method private static b(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method

.method private b(Lcom/jscape/util/a/i;)Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/jscape/ftcl/c;->b()I

    move-result v0

    sget-object v1, Lcom/jscape/ftcl/m;->n:Lcom/jscape/util/a/l;

    invoke-virtual {p1, v1}, Lcom/jscape/util/a/i;->a(Lcom/jscape/util/a/l;)Ljava/lang/String;

    move-result-object p1

    if-nez v0, :cond_4

    if-eqz p1, :cond_3

    :try_start_0
    sget-object v1, Lcom/jscape/ftcl/m;->q:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    :try_start_1
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v1, :cond_2

    :try_start_2
    sget-object v1, Lcom/jscape/ftcl/m;->q:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    if-nez v0, :cond_1

    :try_start_3
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    :try_start_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_4

    :cond_1
    move-object p1, v1

    :cond_2
    :goto_0
    return-object p1

    :catch_0
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/ftcl/m;->b(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/ftcl/m;->b(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_2

    :catch_2
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/ftcl/m;->b(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_3

    :catch_3
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/ftcl/m;->b(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/m;->b(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_3
    sget-object p1, Lcom/jscape/ftcl/m;->q:[Ljava/lang/String;

    const/4 v0, 0x1

    aget-object p1, p1, v0

    :cond_4
    return-object p1
.end method


# virtual methods
.method public a()[Lcom/jscape/util/a/l;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-super {p0}, Lcom/jscape/ftcl/l;->a()[Lcom/jscape/util/a/l;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sget-object v1, Lcom/jscape/ftcl/m;->n:Lcom/jscape/util/a/l;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/jscape/util/a/l;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/util/a/l;

    check-cast v0, [Lcom/jscape/util/a/l;

    return-object v0
.end method

.method public d()Lcom/jscape/filetransfer/FileTransfer;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/ftcl/c;->c()I

    move-result v0

    sget-object v1, Lcom/jscape/ftcl/m;->q:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/jscape/ftcl/m;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/jscape/filetransfer/FtpsImplicitTransfer;

    iget-object v2, p0, Lcom/jscape/ftcl/m;->g:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/util/ConnectionParameters;->getHost()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/ftcl/m;->h:Ljava/lang/String;

    iget-object v4, p0, Lcom/jscape/ftcl/m;->i:Ljava/lang/String;

    iget-object v5, p0, Lcom/jscape/ftcl/m;->g:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v5}, Lcom/jscape/inet/util/ConnectionParameters;->getPort()I

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/jscape/filetransfer/FtpsImplicitTransfer;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    if-nez v0, :cond_1

    :cond_0
    new-instance v1, Lcom/jscape/filetransfer/FtpsTransfer;

    iget-object v0, p0, Lcom/jscape/ftcl/m;->g:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v0}, Lcom/jscape/inet/util/ConnectionParameters;->getHost()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/jscape/ftcl/m;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/jscape/ftcl/m;->i:Ljava/lang/String;

    iget-object v4, p0, Lcom/jscape/ftcl/m;->g:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v4}, Lcom/jscape/inet/util/ConnectionParameters;->getPort()I

    move-result v4

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/jscape/filetransfer/FtpsTransfer;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    :cond_1
    iget-boolean v0, p0, Lcom/jscape/ftcl/m;->j:Z

    invoke-interface {v1, v0}, Lcom/jscape/filetransfer/FileTransfer;->setDebug(Z)Lcom/jscape/filetransfer/FileTransfer;

    return-object v1
.end method
