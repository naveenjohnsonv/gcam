.class public Lcom/jscape/ftcl/l;
.super Ljava/lang/Object;


# static fields
.field protected static final a:Lcom/jscape/util/a/l;

.field protected static final b:Lcom/jscape/util/a/l;

.field protected static final c:Lcom/jscape/util/a/l;

.field protected static final d:Lcom/jscape/util/a/l;

.field protected static final e:I = 0x15

.field private static final p:[Ljava/lang/String;


# instance fields
.field protected f:Lcom/jscape/util/a/i;

.field protected g:Lcom/jscape/inet/util/ConnectionParameters;

.field protected h:Ljava/lang/String;

.field protected i:Ljava/lang/String;

.field protected j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const/16 v0, 0xb

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x7

    const/4 v4, 0x0

    const-string v5, "j*\u0000<\u0016Ly\u0008J*\u0004)\u001d\u00174Z\u0002\u0012)\u0005\u0012)\u000e)\u0007\u0005\u001f1\u000e(\u0007\u0004O6\u0013/\u0007j*\u0000<\u0016Ly\u0005\u001f1\u000e(\u0007\u0002\u0012="

    const/16 v6, 0x35

    move v8, v4

    const/4 v7, -0x1

    :goto_0
    const/4 v9, 0x1

    add-int/2addr v7, v9

    add-int v10, v7, v2

    invoke-virtual {v5, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v12, -0x1

    const/16 v13, 0x9

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v14, v10

    move v15, v4

    :goto_2
    const/4 v0, 0x5

    const/4 v3, 0x3

    const/4 v11, 0x2

    if-gt v14, v15, :cond_3

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v13}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    const/16 v13, 0x8

    if-eqz v12, :cond_1

    add-int/lit8 v0, v8, 0x1

    aput-object v10, v1, v8

    add-int/2addr v7, v2

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v0

    const/16 v0, 0xb

    goto :goto_0

    :cond_0
    const-string v5, "=J`ZvkY)\u0002`^"

    move v8, v0

    move v2, v13

    const/16 v6, 0xb

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v8, 0x1

    aput-object v10, v1, v8

    add-int/2addr v7, v2

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v2, v0

    move v8, v12

    :goto_3
    const/16 v13, 0x7b

    add-int/2addr v7, v9

    add-int v0, v7, v2

    invoke-virtual {v5, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v12, v4

    const/16 v0, 0xb

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/ftcl/l;->p:[Ljava/lang/String;

    new-instance v1, Lcom/jscape/util/a/d;

    sget-object v2, Lcom/jscape/ftcl/l;->p:[Ljava/lang/String;

    aget-object v3, v2, v3

    aget-object v0, v2, v0

    const v5, 0x7fffffff

    invoke-direct {v1, v3, v0, v4, v5}, Lcom/jscape/util/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;ZI)V

    sput-object v1, Lcom/jscape/ftcl/l;->a:Lcom/jscape/util/a/l;

    new-instance v0, Lcom/jscape/util/a/d;

    const/16 v1, 0xa

    aget-object v1, v2, v1

    aget-object v3, v2, v9

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/jscape/util/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;ZI)V

    sput-object v0, Lcom/jscape/ftcl/l;->b:Lcom/jscape/util/a/l;

    new-instance v0, Lcom/jscape/util/a/d;

    aget-object v1, v2, v11

    const/16 v16, 0x9

    aget-object v3, v2, v16

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/jscape/util/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;ZI)V

    sput-object v0, Lcom/jscape/ftcl/l;->c:Lcom/jscape/util/a/l;

    new-instance v0, Lcom/jscape/util/a/f;

    aget-object v1, v2, v13

    invoke-direct {v0, v1, v4, v5}, Lcom/jscape/util/a/f;-><init>(Ljava/lang/String;ZI)V

    sput-object v0, Lcom/jscape/ftcl/l;->d:Lcom/jscape/util/a/l;

    return-void

    :cond_3
    const/16 v16, 0x9

    aget-char v17, v10, v15

    rem-int/lit8 v4, v15, 0x7

    const/16 v18, 0x50

    if-eqz v4, :cond_8

    if-eq v4, v9, :cond_9

    if-eq v4, v11, :cond_7

    if-eq v4, v3, :cond_6

    const/4 v3, 0x4

    if-eq v4, v3, :cond_5

    if-eq v4, v0, :cond_4

    goto :goto_4

    :cond_4
    const/16 v18, 0x7f

    goto :goto_4

    :cond_5
    const/16 v18, 0x7a

    goto :goto_4

    :cond_6
    const/16 v18, 0x52

    goto :goto_4

    :cond_7
    const/16 v18, 0x68

    goto :goto_4

    :cond_8
    const/16 v18, 0x36

    :cond_9
    :goto_4
    xor-int v0, v13, v18

    xor-int v0, v17, v0

    int-to-char v0, v0

    aput-char v0, v10, v15

    add-int/lit8 v15, v15, 0x1

    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method public constructor <init>([Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/c;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/jscape/ftcl/c;->b()I

    move-result v0

    new-instance v1, Lcom/jscape/util/a/i;

    invoke-virtual {p0}, Lcom/jscape/ftcl/l;->a()[Lcom/jscape/util/a/l;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/jscape/util/a/i;-><init>([Lcom/jscape/util/a/l;)V

    iput-object v1, p0, Lcom/jscape/ftcl/l;->f:Lcom/jscape/util/a/i;

    :try_start_0
    array-length v1, p1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v2, 0x0

    if-nez v0, :cond_0

    if-eqz v1, :cond_2

    :try_start_1
    aget-object v1, p1, v2

    if-nez v0, :cond_1

    invoke-static {v1}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4

    :cond_0
    if-eqz v1, :cond_2

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-object v1, p1, v0

    :cond_1
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    new-array v3, v0, [Ljava/lang/String;

    invoke-static {p1, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :try_start_2
    iget-object p1, p0, Lcom/jscape/ftcl/l;->f:Lcom/jscape/util/a/i;

    invoke-virtual {p1, v3}, Lcom/jscape/util/a/i;->a([Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    new-instance p1, Lcom/jscape/inet/util/ConnectionParameters;

    iget-object v0, p0, Lcom/jscape/ftcl/l;->f:Lcom/jscape/util/a/i;

    invoke-direct {p0, v0}, Lcom/jscape/ftcl/l;->a(Lcom/jscape/util/a/i;)I

    move-result v0

    invoke-direct {p1, v1, v0}, Lcom/jscape/inet/util/ConnectionParameters;-><init>(Ljava/lang/String;I)V

    iput-object p1, p0, Lcom/jscape/ftcl/l;->g:Lcom/jscape/inet/util/ConnectionParameters;

    iget-object p1, p0, Lcom/jscape/ftcl/l;->f:Lcom/jscape/util/a/i;

    sget-object v0, Lcom/jscape/ftcl/l;->b:Lcom/jscape/util/a/l;

    invoke-virtual {p1, v0}, Lcom/jscape/util/a/i;->a(Lcom/jscape/util/a/l;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/ftcl/l;->h:Ljava/lang/String;

    iget-object p1, p0, Lcom/jscape/ftcl/l;->f:Lcom/jscape/util/a/i;

    sget-object v0, Lcom/jscape/ftcl/l;->c:Lcom/jscape/util/a/l;

    invoke-virtual {p1, v0}, Lcom/jscape/util/a/i;->a(Lcom/jscape/util/a/l;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/ftcl/l;->i:Ljava/lang/String;

    iget-object p1, p0, Lcom/jscape/ftcl/l;->f:Lcom/jscape/util/a/i;

    sget-object v0, Lcom/jscape/ftcl/l;->d:Lcom/jscape/util/a/l;

    invoke-virtual {p1, v0}, Lcom/jscape/util/a/i;->b(Lcom/jscape/util/a/l;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/jscape/ftcl/l;->j:Z

    return-void

    :catch_0
    new-instance p1, Lcom/jscape/ftcl/c;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/ftcl/l;->p:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/ftcl/l;->f:Lcom/jscape/util/a/i;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/jscape/ftcl/c;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :try_start_3
    new-instance p1, Lcom/jscape/ftcl/c;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/ftcl/l;->p:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/ftcl/l;->f:Lcom/jscape/util/a/i;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/jscape/ftcl/l;->p:[Ljava/lang/String;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/jscape/ftcl/c;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/l;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/ftcl/l;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/ftcl/l;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/l;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
.end method

.method private a(Lcom/jscape/util/a/i;)I
    .locals 2

    invoke-static {}, Lcom/jscape/ftcl/c;->b()I

    move-result v0

    :try_start_0
    sget-object v1, Lcom/jscape/ftcl/l;->a:Lcom/jscape/util/a/l;

    invoke-virtual {p1, v1}, Lcom/jscape/util/a/i;->a(Lcom/jscape/util/a/l;)Ljava/lang/String;

    move-result-object p1

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/16 p1, 0x15

    goto :goto_1

    :cond_1
    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return p1

    :catch_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/l;->h:Ljava/lang/String;

    return-void
.end method

.method public a()[Lcom/jscape/util/a/l;
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/jscape/util/a/l;

    sget-object v1, Lcom/jscape/ftcl/l;->a:Lcom/jscape/util/a/l;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/ftcl/l;->b:Lcom/jscape/util/a/l;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/ftcl/l;->c:Lcom/jscape/util/a/l;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/ftcl/l;->d:Lcom/jscape/util/a/l;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/l;->h:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/l;->i:Ljava/lang/String;

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/l;->i:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lcom/jscape/filetransfer/FileTransfer;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/filetransfer/FtpTransfer;

    iget-object v1, p0, Lcom/jscape/ftcl/l;->g:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getHost()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/ftcl/l;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/jscape/ftcl/l;->i:Ljava/lang/String;

    iget-object v4, p0, Lcom/jscape/ftcl/l;->g:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v4}, Lcom/jscape/inet/util/ConnectionParameters;->getPort()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/jscape/filetransfer/FtpTransfer;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    iget-boolean v1, p0, Lcom/jscape/ftcl/l;->j:Z

    invoke-interface {v0, v1}, Lcom/jscape/filetransfer/FileTransfer;->setDebug(Z)Lcom/jscape/filetransfer/FileTransfer;

    return-object v0
.end method
