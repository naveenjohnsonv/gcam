.class public Lcom/jscape/ftcl/d;
.super Lcom/jscape/filetransfer/FileTransferListener$Adapter;


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x22

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x1

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    const-string v7, "_\u001e\u0012xU\'\u000ejP^t\u0006 ^i\u001b\u0011|\u0011eO9\u0003\u0011=R%X<\u0004Y3Pn$_\u001e\u0012xU\'\u000ejR\r:UiX9\u0013\u0011j\u001blDx\u0013\u001byUtD9P[nR.\u000ew"

    invoke-virtual {v7, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v8, v4

    move v9, v2

    :goto_1
    if-gt v8, v9, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x47

    if-ge v3, v4, :cond_0

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/ftcl/d;->b:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v10, v4, v9

    rem-int/lit8 v11, v9, 0x7

    const/16 v12, 0xe

    if-eqz v11, :cond_7

    if-eq v11, v6, :cond_6

    if-eq v11, v0, :cond_5

    const/4 v13, 0x3

    if-eq v11, v13, :cond_4

    const/4 v13, 0x4

    if-eq v11, v13, :cond_3

    const/4 v13, 0x5

    if-eq v11, v13, :cond_2

    const/16 v11, 0x25

    goto :goto_2

    :cond_2
    move v11, v12

    goto :goto_2

    :cond_3
    const/16 v11, 0x7b

    goto :goto_2

    :cond_4
    const/16 v11, 0x13

    goto :goto_2

    :cond_5
    const/16 v11, 0x70

    goto :goto_2

    :cond_6
    const/16 v11, 0x79

    goto :goto_2

    :cond_7
    const/16 v11, 0x17

    :goto_2
    xor-int/2addr v11, v12

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v4, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/filetransfer/FileTransferListener$Adapter;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/ftcl/d;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public download(Lcom/jscape/filetransfer/FileTransferDownloadEvent;)V
    .locals 6

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v1, Lcom/jscape/ftcl/d;->b:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferDownloadEvent;->getPath()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferDownloadEvent;->getFilename()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v3, v2

    iget-object p1, p0, Lcom/jscape/ftcl/d;->a:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object p1, v3, v2

    invoke-virtual {v0, v1, v3}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    return-void
.end method

.method public upload(Lcom/jscape/filetransfer/FileTransferUploadEvent;)V
    .locals 5

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v1, Lcom/jscape/ftcl/d;->b:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/jscape/ftcl/d;->a:Ljava/lang/String;

    aput-object v4, v3, v2

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferUploadEvent;->getPath()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v3, v4

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferUploadEvent;->getFilename()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x2

    aput-object p1, v3, v2

    invoke-virtual {v0, v1, v3}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    return-void
.end method
