.class public Lcom/jscape/ftcl/f;
.super Lcom/jscape/ftcl/e;


# static fields
.field private static final c:Ljava/lang/String;

.field private static final e:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "r>|!)\u0005r>|!)\nH9{\"`J!|b("

    const/16 v5, 0x16

    move v7, v0

    const/4 v6, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x3c

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x12

    const/4 v4, 0x7

    const-string v6, ";L\u001cX\u0003y\u0015\n+M\u000bE\u001f\"X\u001b\u0004N"

    move v7, v4

    move-object v4, v6

    move v8, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x5a

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/ftcl/f;->e:[Ljava/lang/String;

    aget-object v0, v1, v10

    sput-object v0, Lcom/jscape/ftcl/f;->c:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    const/4 v3, 0x4

    if-eq v2, v3, :cond_5

    if-eq v2, v0, :cond_4

    const/16 v2, 0x6f

    goto :goto_4

    :cond_4
    const/16 v2, 0x19

    goto :goto_4

    :cond_5
    const/16 v2, 0x2b

    goto :goto_4

    :cond_6
    const/16 v2, 0x6d

    goto :goto_4

    :cond_7
    const/16 v2, 0x34

    goto :goto_4

    :cond_8
    const/16 v2, 0x64

    goto :goto_4

    :cond_9
    const/16 v2, 0x24

    :goto_4
    xor-int/2addr v2, v9

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransfer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/ftcl/e;-><init>()V

    invoke-static {}, Lcom/jscape/ftcl/c;->b()I

    move-result v0

    invoke-interface {p1}, Lcom/jscape/filetransfer/FileTransfer;->connect()Lcom/jscape/filetransfer/FileTransfer;

    new-instance v1, Lcom/jscape/ftcl/a/b;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/b;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    :try_start_0
    new-instance v1, Lcom/jscape/ftcl/a/c;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/c;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/d;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/d;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/e;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/e;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/f;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/f;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/g;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/g;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/j;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/j;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/h;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/h;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/i;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/i;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/k;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/k;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/l;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/l;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/t;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/t;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/n;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/n;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/o;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/o;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/p;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/p;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/q;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/q;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/r;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/r;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/s;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/s;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/u;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/u;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/v;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/v;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/w;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/w;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/x;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/x;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/y;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/y;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/z;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/z;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/A;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/A;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/B;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/B;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/C;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/C;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/D;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/D;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/E;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/E;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/F;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/F;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/G;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/G;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/H;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/H;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    new-instance v1, Lcom/jscape/ftcl/a/I;

    invoke-direct {v1, p1}, Lcom/jscape/ftcl/a/I;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/f;->a(Lcom/jscape/ftcl/i;)V

    if-eqz v0, :cond_0

    const/4 p1, 0x2

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/ftcl/c;->c()I

    move-result v0

    :try_start_0
    invoke-static {}, Lcom/jscape/util/a/j;->a()Lcom/jscape/util/a/j;

    move-result-object v1

    new-instance v2, Lcom/jscape/ftcl/l;

    invoke-direct {v2, p0}, Lcom/jscape/ftcl/l;-><init>([Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/ftcl/c; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    :try_start_1
    invoke-virtual {v2}, Lcom/jscape/ftcl/l;->b()Ljava/lang/String;

    move-result-object p0
    :try_end_1
    .catch Lcom/jscape/ftcl/c; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    if-eqz v0, :cond_1

    if-nez p0, :cond_0

    :try_start_2
    sget-object p0, Lcom/jscape/ftcl/f;->e:[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object p0, p0, v3

    invoke-virtual {v1, p0}, Lcom/jscape/util/a/j;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Lcom/jscape/ftcl/l;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/jscape/ftcl/c; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    :cond_0
    if-eqz v0, :cond_2

    :try_start_3
    invoke-virtual {v2}, Lcom/jscape/ftcl/l;->c()Ljava/lang/String;

    move-result-object p0
    :try_end_3
    .catch Lcom/jscape/ftcl/c; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_4
    invoke-static {p0}, Lcom/jscape/ftcl/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    if-nez p0, :cond_3

    :cond_2
    sget-object p0, Lcom/jscape/ftcl/f;->e:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object p0, p0, v3

    invoke-virtual {v1, p0}, Lcom/jscape/util/a/j;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Lcom/jscape/ftcl/l;->b(Ljava/lang/String;)V

    :cond_3
    new-instance p0, Lcom/jscape/ftcl/f;

    invoke-virtual {v2}, Lcom/jscape/ftcl/l;->d()Lcom/jscape/filetransfer/FileTransfer;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/jscape/ftcl/f;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-interface {p0}, Lcom/jscape/ftcl/j;->a()Ljava/lang/String;

    move-result-object v2

    :cond_4
    invoke-virtual {v1, v2}, Lcom/jscape/util/a/j;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3
    :try_end_4
    .catch Lcom/jscape/ftcl/c; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    if-eqz v0, :cond_5

    :try_start_5
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4
    :try_end_5
    .catch Lcom/jscape/ftcl/c; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    if-eqz v0, :cond_7

    if-lez v4, :cond_4

    :try_start_6
    invoke-interface {p0, v3}, Lcom/jscape/ftcl/j;->b(Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/ftcl/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_5
    :goto_1
    if-nez v0, :cond_4

    :cond_6
    const/4 v4, 0x0

    :cond_7
    invoke-static {v4}, Ljava/lang/System;->exit(I)V
    :try_end_6
    .catch Lcom/jscape/ftcl/c; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_3

    :catch_2
    move-exception p0

    :try_start_7
    invoke-static {p0}, Lcom/jscape/ftcl/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_7
    .catch Lcom/jscape/ftcl/c; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    :catch_3
    move-exception p0

    :try_start_8
    invoke-static {p0}, Lcom/jscape/ftcl/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_8
    .catch Lcom/jscape/ftcl/c; {:try_start_8 .. :try_end_8} :catch_5
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    move-exception p0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/ftcl/f;->e:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_2

    :catch_5
    move-exception p0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p0}, Lcom/jscape/ftcl/c;->getMessage()Ljava/lang/String;

    move-result-object p0

    :goto_2
    invoke-virtual {v0, p0}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    :goto_3
    const/4 p0, 0x1

    invoke-static {p0}, Ljava/lang/System;->exit(I)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/jscape/ftcl/f;->e:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method
