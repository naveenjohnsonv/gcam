.class public Lcom/jscape/ftcl/FileTransferAgentTask;
.super Lorg/apache/tools/ant/Task;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private debug:Ljava/lang/Boolean;

.field private hostname:Ljava/lang/String;

.field private keyFile:Ljava/lang/String;

.field private logFile:Ljava/lang/String;

.field private password:Ljava/lang/String;

.field private port:Ljava/lang/Integer;

.field private scriptFile:Ljava/lang/String;

.field private secure:Ljava/lang/Boolean;

.field private timeout:Lcom/jscape/util/Time;

.field private username:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, ":l\u0015/fnQ[v\u001a8(>U|m\u00170jj@o?\u001f./pJi?\u0005-j}L{v\u00139!"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/ftcl/FileTransferAgentTask;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x16

    goto :goto_1

    :cond_1
    const/16 v4, 0x2d

    goto :goto_1

    :cond_2
    const/16 v4, 0x3c

    goto :goto_1

    :cond_3
    const/16 v4, 0x6e

    goto :goto_1

    :cond_4
    const/16 v4, 0x45

    goto :goto_1

    :cond_5
    const/16 v4, 0x2c

    goto :goto_1

    :cond_6
    const/16 v4, 0x2e

    :goto_1
    const/16 v5, 0x33

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->debug:Ljava/lang/Boolean;

    return-void
.end method

.method private static a(Lorg/apache/tools/ant/BuildException;)Lorg/apache/tools/ant/BuildException;
    .locals 0

    return-object p0
.end method

.method private asConfiguration()Lcom/jscape/ftcl/k;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/ftcl/FileTransferAgentTask;->checkParameters()V

    new-instance v12, Lcom/jscape/ftcl/k;

    iget-object v1, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->scriptFile:Ljava/lang/String;

    iget-object v2, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->hostname:Ljava/lang/String;

    iget-object v3, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->port:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->timeout:Lcom/jscape/util/Time;

    iget-object v5, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->username:Ljava/lang/String;

    iget-object v6, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->password:Ljava/lang/String;

    iget-object v7, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->secure:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->keyFile:Ljava/lang/String;

    iget-object v9, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->debug:Ljava/lang/Boolean;

    iget-object v10, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->logFile:Ljava/lang/String;

    new-instance v11, Lcom/jscape/ftcl/b/a/bw;

    invoke-direct {v11}, Lcom/jscape/ftcl/b/a/bw;-><init>()V

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/jscape/ftcl/k;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lcom/jscape/ftcl/b/a/bw;)V

    return-object v12
.end method

.method private checkParameters()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->scriptFile:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/BuildException;

    sget-object v1, Lcom/jscape/ftcl/FileTransferAgentTask;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/ftcl/FileTransferAgentTask;->a(Lorg/apache/tools/ant/BuildException;)Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public execute()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/ftcl/FileTransferAgentTask;->asConfiguration()Lcom/jscape/ftcl/k;

    move-result-object v0

    :try_start_0
    invoke-static {v0}, Lcom/jscape/ftcl/FTCL;->execute(Lcom/jscape/ftcl/k;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v1, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public setDebug(Z)V
    .locals 0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->debug:Ljava/lang/Boolean;

    return-void
.end method

.method public setHostname(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->hostname:Ljava/lang/String;

    return-void
.end method

.method public setKeyFile(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->keyFile:Ljava/lang/String;

    return-void
.end method

.method public setLogFile(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->logFile:Ljava/lang/String;

    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->password:Ljava/lang/String;

    return-void
.end method

.method public setPort(I)V
    .locals 0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->port:Ljava/lang/Integer;

    return-void
.end method

.method public setScriptFile(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->scriptFile:Ljava/lang/String;

    return-void
.end method

.method public setSecure(Z)V
    .locals 0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->secure:Ljava/lang/Boolean;

    return-void
.end method

.method public setTimeout(I)V
    .locals 2

    int-to-long v0, p1

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->seconds(J)Lcom/jscape/util/Time;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->timeout:Lcom/jscape/util/Time;

    return-void
.end method

.method public setUsername(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/FileTransferAgentTask;->username:Ljava/lang/String;

    return-void
.end method
