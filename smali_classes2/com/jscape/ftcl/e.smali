.class public abstract Lcom/jscape/ftcl/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/ftcl/j;


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field private a:Ljava/util/Map;

.field private b:Lcom/jscape/ftcl/i;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x9

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x10

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\u0014\u0011x#z\u0000)X\u0007\u0016}\u0019a06\u0001(\u0014\u0014x<7\t\"PY7\u0005#\u0018)\u0014"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x20

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/ftcl/e;->d:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x5c

    goto :goto_2

    :cond_2
    const/16 v12, 0x78

    goto :goto_2

    :cond_3
    const/16 v12, 0x4a

    goto :goto_2

    :cond_4
    const/16 v12, 0x41

    goto :goto_2

    :cond_5
    const/4 v12, 0x7

    goto :goto_2

    :cond_6
    const/16 v12, 0x67

    goto :goto_2

    :cond_7
    const/16 v12, 0x24

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/ftcl/e;->a:Ljava/util/Map;

    new-instance v0, Lcom/jscape/ftcl/a/m;

    invoke-direct {v0, p0}, Lcom/jscape/ftcl/a/m;-><init>(Lcom/jscape/ftcl/j;)V

    iput-object v0, p0, Lcom/jscape/ftcl/e;->b:Lcom/jscape/ftcl/i;

    invoke-virtual {p0, v0}, Lcom/jscape/ftcl/e;->a(Lcom/jscape/ftcl/i;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/jscape/ftcl/i;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/e;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/ftcl/i;

    return-object p1
.end method

.method public a(Lcom/jscape/ftcl/i;)V
    .locals 3

    invoke-static {}, Lcom/jscape/ftcl/c;->c()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/ftcl/e;->a:Ljava/util/Map;

    invoke-interface {p1}, Lcom/jscape/ftcl/i;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Lcom/jscape/ftcl/i;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/jscape/ftcl/e;->a:Ljava/util/Map;

    invoke-interface {p1}, Lcom/jscape/ftcl/i;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 6

    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, " "

    invoke-direct {v0, p1, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/ftcl/c;->b()I

    move-result p1

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    move v4, v3

    :cond_0
    if-ge v4, v1, :cond_1

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    if-nez p1, :cond_5

    if-eqz p1, :cond_0

    :cond_1
    if-nez p1, :cond_2

    if-eqz v1, :cond_5

    :cond_2
    aget-object v0, v2, v3

    invoke-virtual {p0, v0}, Lcom/jscape/ftcl/e;->a(Ljava/lang/String;)Lcom/jscape/ftcl/i;

    move-result-object v0

    if-nez p1, :cond_3

    if-eqz v0, :cond_4

    :cond_3
    invoke-interface {v0, v2}, Lcom/jscape/ftcl/i;->a([Ljava/lang/String;)V

    if-eqz p1, :cond_5

    :cond_4
    sget-object p1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/ftcl/e;->d:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/ftcl/e;->b:Lcom/jscape/ftcl/i;

    invoke-interface {v2}, Lcom/jscape/ftcl/i;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method public b()[Lcom/jscape/ftcl/i;
    .locals 2

    iget-object v0, p0, Lcom/jscape/ftcl/e;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    new-array v1, v1, [Lcom/jscape/ftcl/i;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/ftcl/i;

    check-cast v0, [Lcom/jscape/ftcl/i;

    return-object v0
.end method
