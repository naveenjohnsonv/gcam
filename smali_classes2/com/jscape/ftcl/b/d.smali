.class public Lcom/jscape/ftcl/b/d;
.super Lcom/jscape/filetransfer/FileTransferParameters;


# static fields
.field private static n:I

.field private static final p:[Ljava/lang/String;


# instance fields
.field private a:Ljava/io/File;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Boolean;

.field private d:Lcom/jscape/filetransfer/TransferMode;

.field private e:Ljava/lang/String;

.field private f:Lcom/jscape/filetransfer/AftpSecurityMode;

.field private g:Lcom/jscape/util/A;

.field private h:Lcom/jscape/util/A;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Boolean;

.field private k:[Ljava/lang/String;

.field private l:Ljava/lang/Long;

.field private m:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/jscape/ftcl/b/d;->b(I)V

    const/4 v3, -0x1

    const-string v4, "\u0013(tLMN\u00010(EaeT\u000b0\u000egayF\u00000\u0008Rrx\u0007\u00166\u0010^eeS&0\u0005qigBP\u0011y\\Bsnw\u000c&\u000f^vnj\u00021\u0019\n(y\\Vf\u007fW.:\u0011GrnT\u001e<\u0013YEsD\u0001 \u0018RdMN\u000109OtnI\u001e<\u0013Ys6\u0015y\\Vf\u007fW8&\u0019tofW\u001f0\u000fDidIP\u000fy\\CrjI\u001e3\u0019EMdC\u0008h\u0019y\\Df\u007fW=<\u000cRlbI\u0004;\u001beezR\u0004\'\u0019S=\u0013y\\Vf\u007fW>0\u001fBrbS\u0014\u0018\u0013Se6\u0010y\\@iyB(;\u001fXdbI\nh[\u001by\\Vf\u007fW8&\u0019toe@\u0008&\u0008^oed\u0002;\u0008Eog\u001a\u0016y\\Vf\u007fW):\u000bYldF\t\u0017\u0015CRjS\u0008h"

    const/16 v5, 0xfd

    move v8, v1

    move v6, v3

    const/16 v7, 0x2a

    :goto_0
    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v7

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0x47

    move v12, v3

    move v13, v11

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v14, v10

    move v15, v1

    :goto_2
    if-gt v14, v15, :cond_3

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v13}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v12, :cond_1

    add-int/lit8 v12, v8, 0x1

    aput-object v10, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v12

    goto :goto_0

    :cond_0
    const/16 v5, 0x32

    const/16 v4, 0x14

    const-string v6, "\u0018=7\u0007\u001e6YDq9\u0000\u000e\u0004e@O7\u0015\u000f{\u001d\u0018=7\u0007\u001e6O[p&\u0013\u000f5\u007f]r8,\u0003(J]q32\u0003<i\t"

    move v7, v4

    move-object v4, v6

    move v8, v12

    move v6, v3

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v8, 0x1

    aput-object v10, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v12

    :goto_3
    const/16 v13, 0x26

    add-int/2addr v6, v9

    add-int v10, v6, v7

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v12, v1

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/ftcl/b/d;->p:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v10, v15

    rem-int/lit8 v1, v15, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v9, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v1, 0x2a

    goto :goto_4

    :cond_4
    const/16 v1, 0x60

    goto :goto_4

    :cond_5
    const/16 v1, 0x4c

    goto :goto_4

    :cond_6
    move v1, v11

    goto :goto_4

    :cond_7
    const/16 v1, 0x70

    goto :goto_4

    :cond_8
    const/16 v1, 0x3b

    goto :goto_4

    :cond_9
    const/16 v1, 0x12

    :goto_4
    xor-int/2addr v1, v13

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v10, v15

    add-int/lit8 v15, v15, 0x1

    const/4 v1, 0x0

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/Protocol;Ljava/lang/String;Ljava/lang/Integer;Lcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/io/PrintStream;Ljava/io/File;Ljava/io/File;Ljava/lang/String;Ljava/lang/Boolean;Lcom/jscape/filetransfer/TransferMode;Ljava/lang/String;Lcom/jscape/filetransfer/AftpSecurityMode;Lcom/jscape/util/A;Lcom/jscape/util/A;Ljava/lang/Boolean;Ljava/lang/Boolean;[Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Boolean;)V
    .locals 3

    move-object v0, p0

    invoke-direct/range {p0 .. p11}, Lcom/jscape/filetransfer/FileTransferParameters;-><init>(Lcom/jscape/filetransfer/Protocol;Ljava/lang/String;Ljava/lang/Integer;Lcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/io/PrintStream;Ljava/io/File;)V

    invoke-static {}, Lcom/jscape/ftcl/b/d;->n()I

    move-result v1

    move-object v2, p12

    iput-object v2, v0, Lcom/jscape/ftcl/b/d;->a:Ljava/io/File;

    move-object/from16 v2, p13

    iput-object v2, v0, Lcom/jscape/ftcl/b/d;->b:Ljava/lang/String;

    move-object/from16 v2, p14

    iput-object v2, v0, Lcom/jscape/ftcl/b/d;->c:Ljava/lang/Boolean;

    move-object/from16 v2, p15

    iput-object v2, v0, Lcom/jscape/ftcl/b/d;->d:Lcom/jscape/filetransfer/TransferMode;

    move-object/from16 v2, p16

    iput-object v2, v0, Lcom/jscape/ftcl/b/d;->e:Ljava/lang/String;

    move-object/from16 v2, p17

    iput-object v2, v0, Lcom/jscape/ftcl/b/d;->f:Lcom/jscape/filetransfer/AftpSecurityMode;

    move-object/from16 v2, p18

    iput-object v2, v0, Lcom/jscape/ftcl/b/d;->g:Lcom/jscape/util/A;

    move-object/from16 v2, p19

    iput-object v2, v0, Lcom/jscape/ftcl/b/d;->h:Lcom/jscape/util/A;

    move-object/from16 v2, p20

    iput-object v2, v0, Lcom/jscape/ftcl/b/d;->i:Ljava/lang/Boolean;

    move-object/from16 v2, p21

    iput-object v2, v0, Lcom/jscape/ftcl/b/d;->j:Ljava/lang/Boolean;

    move-object/from16 v2, p22

    iput-object v2, v0, Lcom/jscape/ftcl/b/d;->k:[Ljava/lang/String;

    move-object/from16 v2, p23

    iput-object v2, v0, Lcom/jscape/ftcl/b/d;->l:Ljava/lang/Long;

    move-object/from16 v2, p24

    iput-object v2, v0, Lcom/jscape/ftcl/b/d;->m:Ljava/lang/Boolean;

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v2

    if-nez v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Lcom/jscape/ftcl/b/d;->b(I)V

    :cond_0
    return-void
.end method

.method private static b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;
    .locals 0

    return-object p0
.end method

.method public static b(I)V
    .locals 0

    sput p0, Lcom/jscape/ftcl/b/d;->n:I

    return-void
.end method

.method public static n()I
    .locals 1

    sget v0, Lcom/jscape/ftcl/b/d;->n:I

    return v0
.end method

.method public static o()I
    .locals 1

    invoke-static {}, Lcom/jscape/ftcl/b/d;->n()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x77

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public a()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/b/d;->a:Ljava/io/File;

    return-object v0
.end method

.method public a(Lcom/jscape/filetransfer/AftpFileTransfer;)V
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    move-object/from16 v0, p0

    new-instance v11, Lcom/jscape/filetransfer/AftpFileTransferParameters;

    move-object v1, v11

    iget-object v12, v0, Lcom/jscape/ftcl/b/d;->f:Lcom/jscape/filetransfer/AftpSecurityMode;

    iget-object v13, v0, Lcom/jscape/ftcl/b/d;->a:Ljava/io/File;

    iget-object v14, v0, Lcom/jscape/ftcl/b/d;->b:Ljava/lang/String;

    iget-object v15, v0, Lcom/jscape/ftcl/b/d;->g:Lcom/jscape/util/A;

    iget-object v2, v0, Lcom/jscape/ftcl/b/d;->h:Lcom/jscape/util/A;

    move-object/from16 v16, v2

    iget-object v2, v0, Lcom/jscape/ftcl/b/d;->i:Ljava/lang/Boolean;

    move-object/from16 v17, v2

    iget-object v2, v0, Lcom/jscape/ftcl/b/d;->j:Ljava/lang/Boolean;

    move-object/from16 v18, v2

    iget-object v2, v0, Lcom/jscape/ftcl/b/d;->k:[Ljava/lang/String;

    move-object/from16 v19, v2

    iget-object v2, v0, Lcom/jscape/ftcl/b/d;->l:Ljava/lang/Long;

    move-object/from16 v20, v2

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v21, 0x0

    move-object/from16 v22, v11

    move-object/from16 v11, v21

    invoke-direct/range {v1 .. v21}, Lcom/jscape/filetransfer/AftpFileTransferParameters;-><init>(Ljava/lang/String;Ljava/lang/Integer;Lcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/io/PrintStream;Ljava/io/File;Lcom/jscape/filetransfer/AftpSecurityMode;Ljava/io/File;Ljava/lang/String;Lcom/jscape/util/A;Lcom/jscape/util/A;Ljava/lang/Boolean;Ljava/lang/Boolean;[Ljava/lang/String;Ljava/lang/Long;Ljava/util/TimeZone;)V

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-virtual {v2, v1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->applySpecificTo(Lcom/jscape/filetransfer/AftpFileTransfer;)V

    return-void
.end method

.method public a(Lcom/jscape/filetransfer/AftpSecurityMode;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/b/d;->f:Lcom/jscape/filetransfer/AftpSecurityMode;

    return-void
.end method

.method public a(Lcom/jscape/filetransfer/FtpsTransfer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/ftcl/b/d;->a:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/ftcl/b/d;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/ftcl/b/d;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/jscape/filetransfer/FtpsTransfer;->setClientCertificates(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/filetransfer/FtpsTransfer;
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/d;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public a(Lcom/jscape/filetransfer/SftpFileTransfer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/ftcl/b/d;->o()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/ftcl/b/d;->a:Ljava/io/File;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/jscape/ftcl/b/d;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jscape/ftcl/b/d;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/jscape/filetransfer/SftpFileTransfer;->setKeyPair(Ljava/io/File;Ljava/lang/String;)Lcom/jscape/filetransfer/SftpFileTransfer;
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_3

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/jscape/ftcl/b/d;->m:Ljava/lang/Boolean;
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    iget-object v1, p0, Lcom/jscape/ftcl/b/d;->m:Ljava/lang/Boolean;
    :try_end_2
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_1
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1
    :try_end_3
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_3 .. :try_end_3} :catch_0

    if-eqz v0, :cond_4

    if-eqz v1, :cond_3

    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->setPipelinedTransferEnabled(Z)Lcom/jscape/filetransfer/SftpFileTransfer;

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/d;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/ftcl/b/d;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/d;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/d;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public a(Lcom/jscape/filetransfer/TransferMode;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/b/d;->d:Lcom/jscape/filetransfer/TransferMode;

    return-void
.end method

.method public a(Lcom/jscape/util/A;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/b/d;->g:Lcom/jscape/util/A;

    return-void
.end method

.method public a(Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/b/d;->a:Ljava/io/File;

    return-void
.end method

.method public a(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/b/d;->c:Ljava/lang/Boolean;

    return-void
.end method

.method public a(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/b/d;->l:Ljava/lang/Long;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/b/d;->b:Ljava/lang/String;

    return-void
.end method

.method public a([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/b/d;->k:[Ljava/lang/String;

    return-void
.end method

.method public applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/ftcl/b/d;->o()I

    move-result v0

    :try_start_0
    invoke-super {p0, p1}, Lcom/jscape/filetransfer/FileTransferParameters;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jscape/ftcl/b/d;->c:Ljava/lang/Boolean;
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_7

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/ftcl/b/d;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {p1, v1}, Lcom/jscape/filetransfer/FileTransfer;->setPassive(Z)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_8

    :cond_0
    if-eqz v0, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/ftcl/b/d;->d:Lcom/jscape/filetransfer/TransferMode;
    :try_end_2
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_2 .. :try_end_2} :catch_0

    if-eqz v1, :cond_1

    :try_start_3
    iget-object v1, p0, Lcom/jscape/ftcl/b/d;->d:Lcom/jscape/filetransfer/TransferMode;

    invoke-virtual {v1, p1}, Lcom/jscape/filetransfer/TransferMode;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/d;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/d;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    :try_start_4
    iget-object v1, p0, Lcom/jscape/ftcl/b/d;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jscape/ftcl/b/d;->e:Ljava/lang/String;

    invoke-interface {p1, v1}, Lcom/jscape/filetransfer/FileTransfer;->setWireEncoding(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_4
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_4 .. :try_end_4} :catch_6

    :cond_2
    :try_start_5
    instance-of v1, p1, Lcom/jscape/filetransfer/AftpFileTransfer;
    :try_end_5
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_5 .. :try_end_5} :catch_4

    if-eqz v0, :cond_4

    if-eqz v1, :cond_3

    :try_start_6
    move-object v1, p1

    check-cast v1, Lcom/jscape/filetransfer/AftpFileTransfer;

    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/b/d;->a(Lcom/jscape/filetransfer/AftpFileTransfer;)V
    :try_end_6
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_6 .. :try_end_6} :catch_5

    :cond_3
    instance-of v1, p1, Lcom/jscape/filetransfer/FtpsTransfer;

    :cond_4
    if-eqz v0, :cond_6

    if-eqz v1, :cond_5

    :try_start_7
    move-object v0, p1

    check-cast v0, Lcom/jscape/filetransfer/FtpsTransfer;

    invoke-virtual {p0, v0}, Lcom/jscape/ftcl/b/d;->a(Lcom/jscape/filetransfer/FtpsTransfer;)V
    :try_end_7
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/d;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_5
    :goto_1
    instance-of v1, p1, Lcom/jscape/filetransfer/SftpFileTransfer;

    :cond_6
    if-eqz v1, :cond_7

    :try_start_8
    check-cast p1, Lcom/jscape/filetransfer/SftpFileTransfer;

    invoke-virtual {p0, p1}, Lcom/jscape/ftcl/b/d;->a(Lcom/jscape/filetransfer/SftpFileTransfer;)V
    :try_end_8
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_2

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/d;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_7
    :goto_2
    return-object p0

    :catch_4
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/ftcl/b/d;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_9 .. :try_end_9} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/d;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/d;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :catch_7
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/ftcl/b/d;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_a .. :try_end_a} :catch_8

    :catch_8
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/d;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/b/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b(Lcom/jscape/util/A;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/b/d;->h:Lcom/jscape/util/A;

    return-void
.end method

.method public b(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/b/d;->i:Ljava/lang/Boolean;

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/b/d;->e:Ljava/lang/String;

    return-void
.end method

.method public c()Lcom/jscape/filetransfer/TransferMode;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/b/d;->d:Lcom/jscape/filetransfer/TransferMode;

    return-object v0
.end method

.method public c(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/b/d;->j:Ljava/lang/Boolean;

    return-void
.end method

.method public d()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/b/d;->c:Ljava/lang/Boolean;

    return-object v0
.end method

.method public d(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/b/d;->m:Ljava/lang/Boolean;

    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/b/d;->e:Ljava/lang/String;

    return-object v0
.end method

.method public f()Lcom/jscape/filetransfer/AftpSecurityMode;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/b/d;->f:Lcom/jscape/filetransfer/AftpSecurityMode;

    return-object v0
.end method

.method public g()Lcom/jscape/util/A;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/b/d;->g:Lcom/jscape/util/A;

    return-object v0
.end method

.method public h()Lcom/jscape/util/A;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/b/d;->h:Lcom/jscape/util/A;

    return-object v0
.end method

.method public i()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/b/d;->i:Ljava/lang/Boolean;

    return-object v0
.end method

.method public j()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/b/d;->j:Ljava/lang/Boolean;

    return-object v0
.end method

.method public k()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/b/d;->k:[Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/b/d;->l:Ljava/lang/Long;

    return-object v0
.end method

.method public m()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/b/d;->m:Ljava/lang/Boolean;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/jscape/ftcl/b/d;->n()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/ftcl/b/d;->p:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/ftcl/b/d;->a:Ljava/io/File;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/ftcl/b/d;->c:Ljava/lang/Boolean;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x4

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/ftcl/b/d;->d:Lcom/jscape/filetransfer/TransferMode;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x7

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/ftcl/b/d;->e:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x27

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x6

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/ftcl/b/d;->f:Lcom/jscape/filetransfer/AftpSecurityMode;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v3, 0xa

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/ftcl/b/d;->g:Lcom/jscape/util/A;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v3, 0x9

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/ftcl/b/d;->h:Lcom/jscape/util/A;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v3, 0x8

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/ftcl/b/d;->i:Ljava/lang/Boolean;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x3

    aget-object v4, v2, v3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/ftcl/b/d;->j:Ljava/lang/Boolean;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v4, 0x2

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/ftcl/b/d;->k:[Ljava/lang/String;

    invoke-static {v4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0xb

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/ftcl/b/d;->l:Ljava/lang/Long;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v4, 0x5

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/ftcl/b/d;->m:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    new-array v0, v3, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object v1
.end method
