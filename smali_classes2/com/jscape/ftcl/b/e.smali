.class public Lcom/jscape/ftcl/b/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/ftcl/b/f;


# instance fields
.field private final a:Lcom/jscape/filetransfer/FileTransferFactory;

.field private final b:Lcom/jscape/util/a/j;


# direct methods
.method public constructor <init>(Lcom/jscape/filetransfer/FileTransferFactory;Lcom/jscape/util/a/j;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/ftcl/b/e;->a:Lcom/jscape/filetransfer/FileTransferFactory;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/ftcl/b/e;->b:Lcom/jscape/util/a/j;

    return-void
.end method

.method private static a(Lcom/jscape/ftcl/b/a/z;)Lcom/jscape/ftcl/b/a/z;
    .locals 0

    return-object p0
.end method

.method public static a()Lcom/jscape/ftcl/b/f;
    .locals 3

    new-instance v0, Lcom/jscape/ftcl/b/e;

    invoke-static {}, Lcom/jscape/filetransfer/ComponentFileTransferFactory;->defaultFactory()Lcom/jscape/filetransfer/ComponentFileTransferFactory;

    move-result-object v1

    invoke-static {}, Lcom/jscape/util/a/j;->a()Lcom/jscape/util/a/j;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/jscape/ftcl/b/e;-><init>(Lcom/jscape/filetransfer/FileTransferFactory;Lcom/jscape/util/a/j;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/InputStream;Lcom/jscape/ftcl/b/d;Lcom/jscape/ftcl/b/a/bw;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/b;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/ftcl/b/d;->o()I

    move-result v0

    new-instance v1, Lcom/jscape/ftcl/b/g;

    iget-object v2, p0, Lcom/jscape/ftcl/b/e;->a:Lcom/jscape/filetransfer/FileTransferFactory;

    iget-object v3, p0, Lcom/jscape/ftcl/b/e;->b:Lcom/jscape/util/a/j;

    invoke-direct {v1, p2, v2, p3, v3}, Lcom/jscape/ftcl/b/g;-><init>(Lcom/jscape/ftcl/b/d;Lcom/jscape/filetransfer/FileTransferFactory;Lcom/jscape/ftcl/b/a/bw;Lcom/jscape/util/a/j;)V

    :try_start_0
    new-instance p2, Lcom/jscape/ftcl/b/a/W;

    sget-object v2, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v2}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p2, p1, v2}, Lcom/jscape/ftcl/b/a/W;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-virtual {p2, p3}, Lcom/jscape/ftcl/b/a/W;->a(Lcom/jscape/ftcl/b/a/bw;)V

    invoke-virtual {p2}, Lcom/jscape/ftcl/b/a/W;->a()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/jscape/ftcl/b/a/aC;
    :try_end_0
    .catch Lcom/jscape/ftcl/b/a/z; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1, p2}, Lcom/jscape/ftcl/b/g;->a(Lcom/jscape/ftcl/b/a/aC;)V
    :try_end_1
    .catch Lcom/jscape/ftcl/b/a/z; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_2

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/ftcl/b/e;->a(Lcom/jscape/ftcl/b/a/z;)Lcom/jscape/ftcl/b/a/z;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/ftcl/b/a/z; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_0
    invoke-virtual {v1}, Lcom/jscape/ftcl/b/g;->a()V

    :cond_2
    return-void

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_3
    new-instance p2, Lcom/jscape/ftcl/b/b;

    invoke-direct {p2, p1}, Lcom/jscape/ftcl/b/b;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :catch_2
    move-exception p1

    new-instance p2, Lcom/jscape/ftcl/b/c;

    invoke-virtual {p1}, Lcom/jscape/ftcl/b/a/z;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/ftcl/b/c;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_1
    invoke-virtual {v1}, Lcom/jscape/ftcl/b/g;->a()V

    throw p1
.end method
