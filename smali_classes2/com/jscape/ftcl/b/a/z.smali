.class public Lcom/jscape/ftcl/b/a/z;
.super Ljava/lang/Exception;


# static fields
.field private static final e:[Ljava/lang/String;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public a:Lcom/jscape/ftcl/b/a/bs;

.field public b:[[I

.field public c:[Ljava/lang/String;

.field protected d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x2

    const/4 v3, 0x0

    const-string v4, "Z\u0014\u0004&R\u0012%\u0003(\\\u001c\u000eQ\u0013A%PlQc\u0011Fl[s\u001b\u0002&P\u0002Z.\u0002ZP\n$RSq\u0015xHh\u0017\u0012\rC\u001cQj@zUc\u0000Wa\u00156\u0002&P\u000ej\u001b\\`\u001bgDv\u0013@dA{S\u0002Z\u001c\u0002Z\u0010\u00046B\u00025\u000ej\u001b\\`\u001bgDv\u0013@dA{S\t*RQjYaLhR\u0002Z\u0007\u0015Q\u0013A%PlQc\u0011Fl[s\u0001i\u001cW%Zr\u001b\u0002Z\u0000\u0004&R\u0012%"

    const/16 v5, 0x93

    move v7, v2

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0xe

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x5

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const-string v4, "\u0001\u000e\u0002\u0001]"

    move v7, v2

    move v8, v11

    move v5, v15

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x55

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v10, :cond_8

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    if-eq v1, v15, :cond_4

    const/16 v1, 0x2f

    goto :goto_4

    :cond_4
    const/16 v1, 0x1a

    goto :goto_4

    :cond_5
    const/16 v1, 0x3b

    goto :goto_4

    :cond_6
    const/16 v1, 0xb

    goto :goto_4

    :cond_7
    const/16 v1, 0x3c

    goto :goto_4

    :cond_8
    const/16 v1, 0x7c

    goto :goto_4

    :cond_9
    const/16 v1, 0x8

    :goto_4
    xor-int/2addr v1, v9

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v2, 0x2

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    sget-object v0, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    const/16 v1, 0xe

    aget-object v0, v0, v1

    const-string v1, "\n"

    invoke-static {v0, v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/ftcl/b/a/z;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/jscape/ftcl/b/a/bs;[[I[Ljava/lang/String;)V
    .locals 2

    invoke-static {p1, p2, p3}, Lcom/jscape/ftcl/b/a/z;->a(Lcom/jscape/ftcl/b/a/bs;[[I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    const/16 v1, 0xe

    aget-object v0, v0, v1

    const-string v1, "\n"

    invoke-static {v0, v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/ftcl/b/a/z;->d:Ljava/lang/String;

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/z;->a:Lcom/jscape/ftcl/b/a/bs;

    iput-object p2, p0, Lcom/jscape/ftcl/b/a/z;->b:[[I

    iput-object p3, p0, Lcom/jscape/ftcl/b/a/z;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    sget-object p1, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    const/16 v0, 0xe

    aget-object p1, p1, v0

    const-string v0, "\n"

    invoke-static {p1, v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/z;->d:Ljava/lang/String;

    return-void
.end method

.method private static a(Lcom/jscape/ftcl/b/a/bs;[[I[Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    sget-object v0, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    const/16 v1, 0xa

    aget-object v0, v0, v1

    const-string v1, "\n"

    invoke-static {v0, v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {}, Lcom/jscape/ftcl/b/a/aC;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move v4, v3

    move v5, v4

    :cond_0
    array-length v6, p1

    const/4 v7, 0x1

    if-ge v4, v6, :cond_7

    if-nez v2, :cond_2

    aget-object v6, p1, v4

    array-length v6, v6

    if-ge v5, v6, :cond_1

    aget-object v5, p1, v4

    array-length v5, v5

    :cond_1
    move v6, v5

    move v5, v3

    goto :goto_0

    :cond_2
    move v6, v5

    :cond_3
    :goto_0
    aget-object v8, p1, v4

    array-length v8, v8

    if-ge v5, v8, :cond_4

    aget-object v8, p1, v4

    aget v8, v8, v5

    aget-object v8, p2, v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v8, 0x20

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v5, v5, 0x1

    if-nez v2, :cond_6

    if-eqz v2, :cond_3

    :cond_4
    aget-object v5, p1, v4

    aget-object v8, p1, v4

    array-length v8, v8

    sub-int/2addr v8, v7

    aget v5, v5, v8

    if-eqz v5, :cond_5

    sget-object v5, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    const/4 v8, 0x2

    aget-object v5, v5, v8

    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v5, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    aget-object v5, v5, v7

    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v4, v4, 0x1

    :cond_6
    move v5, v6

    if-eqz v2, :cond_0

    :cond_7
    sget-object v4, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    const/16 v6, 0x8

    aget-object v4, v4, v6

    iget-object v6, p0, Lcom/jscape/ftcl/b/a/bs;->g:Lcom/jscape/ftcl/b/a/bs;

    move v8, v3

    :cond_8
    if-ge v8, v5, :cond_d

    if-nez v2, :cond_e

    const-string v9, " "

    if-nez v2, :cond_a

    if-eqz v8, :cond_9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_9
    if-nez v2, :cond_c

    iget v10, v6, Lcom/jscape/ftcl/b/a/bs;->a:I

    goto :goto_1

    :cond_a
    move v10, v8

    :goto_1
    if-nez v10, :cond_b

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v4, p2, v3

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    if-eqz v2, :cond_d

    :cond_b
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, v6, Lcom/jscape/ftcl/b/a/bs;->a:I

    aget-object v4, p2, v4

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    const/4 v10, 0x4

    aget-object v10, v4, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v9, v6, Lcom/jscape/ftcl/b/a/bs;->f:Ljava/lang/String;

    invoke-static {v9}, Lcom/jscape/ftcl/b/a/z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v9, 0x9

    aget-object v4, v4, v9

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v6, v6, Lcom/jscape/ftcl/b/a/bs;->g:Lcom/jscape/ftcl/b/a/bs;

    :cond_c
    add-int/lit8 v8, v8, 0x1

    if-eqz v2, :cond_8

    :cond_d
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    const/4 v4, 0x7

    aget-object v4, v3, v4

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/ftcl/b/a/bs;->g:Lcom/jscape/ftcl/b/a/bs;

    iget v4, v4, Lcom/jscape/ftcl/b/a/bs;->b:I

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v4, 0xf

    aget-object v3, v3, v4

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/jscape/ftcl/b/a/bs;->g:Lcom/jscape/ftcl/b/a/bs;

    iget p0, p0, Lcom/jscape/ftcl/b/a/bs;->c:I

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "."

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    if-nez v2, :cond_10

    array-length v8, p1

    :cond_e
    const/16 p0, 0x13

    if-ne v8, v7, :cond_f

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p2, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v3, p2, v3

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object p2, p2, p0

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    if-eqz v2, :cond_10

    :cond_f
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p2, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    const/16 v2, 0x11

    aget-object v2, p2, v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object p0, p2, p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_10
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {}, Lcom/jscape/ftcl/b/a/aC;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move v3, v2

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_c

    if-nez v1, :cond_d

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0xd

    if-nez v1, :cond_7

    const/16 v6, 0xc

    if-eqz v4, :cond_1

    const/16 v7, 0x22

    if-eq v4, v7, :cond_4

    const/16 v7, 0x27

    if-eq v4, v7, :cond_5

    const/16 v7, 0x5c

    if-eq v4, v7, :cond_6

    if-eq v4, v6, :cond_2

    if-eq v4, v5, :cond_3

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_b

    :pswitch_0
    sget-object v4, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    aget-object v4, v4, v6

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz v1, :cond_b

    :pswitch_1
    sget-object v4, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    const/16 v6, 0x15

    aget-object v4, v4, v6

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz v1, :cond_b

    :pswitch_2
    sget-object v4, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    const/16 v6, 0xb

    aget-object v4, v4, v6

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz v1, :cond_b

    :cond_2
    sget-object v4, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz v1, :cond_b

    :cond_3
    sget-object v4, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    const/16 v6, 0x12

    aget-object v4, v4, v6

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz v1, :cond_b

    :cond_4
    sget-object v4, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    const/4 v6, 0x6

    aget-object v4, v4, v6

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz v1, :cond_b

    :cond_5
    sget-object v4, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    const/16 v6, 0x14

    aget-object v4, v4, v6

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz v1, :cond_b

    :cond_6
    sget-object v4, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    const/4 v6, 0x5

    aget-object v4, v4, v6

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz v1, :cond_b

    :goto_0
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    :cond_7
    const/16 v6, 0x20

    if-nez v1, :cond_8

    if-lt v4, v6, :cond_9

    const/16 v6, 0x7e

    :cond_8
    if-le v4, v6, :cond_a

    :cond_9
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/jscape/ftcl/b/a/z;->e:[Ljava/lang/String;

    aget-object v5, v7, v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v5, v7, v5

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x4

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz v1, :cond_b

    :cond_a
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_b
    add-int/lit8 v3, v3, 0x1

    if-eqz v1, :cond_0

    :cond_c
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_d
    return-object p0

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
