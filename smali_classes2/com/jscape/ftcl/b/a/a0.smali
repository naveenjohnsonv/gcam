.class public Lcom/jscape/ftcl/b/a/a0;
.super Lcom/jscape/ftcl/b/a/aC;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/ftcl/b/a/aC<",
        "Lcom/jscape/ftcl/b/a/ad;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/String;

.field public final c:Lcom/jscape/ftcl/b/a/a/j;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x13

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x1

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    const-string v7, "E~\n/2q\u0002\u001d\u001b\u0002-/y\u0001\u001a7\u00153`\u001b9,\u00150-h!\u001d?\u000e80y\u001c\u001d~\u0001+<n\u001b\u0008<\u00168`;"

    invoke-virtual {v7, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v8, v4

    move v9, v2

    :goto_1
    if-gt v8, v9, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x2f

    if-ge v3, v4, :cond_0

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/ftcl/b/a/a0;->d:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v10, v4, v9

    rem-int/lit8 v11, v9, 0x7

    const/16 v12, 0x5e

    const/4 v13, 0x3

    if-eqz v11, :cond_5

    if-eq v11, v6, :cond_4

    if-eq v11, v0, :cond_3

    if-eq v11, v13, :cond_6

    const/4 v14, 0x4

    if-eq v11, v14, :cond_6

    const/4 v12, 0x5

    if-eq v11, v12, :cond_2

    const/16 v12, 0x71

    goto :goto_2

    :cond_2
    const/16 v12, 0x1f

    goto :goto_2

    :cond_3
    const/16 v12, 0x79

    goto :goto_2

    :cond_4
    const/16 v12, 0x5d

    goto :goto_2

    :cond_5
    const/16 v12, 0x6a

    :cond_6
    :goto_2
    xor-int v11, v13, v12

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v4, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/jscape/ftcl/b/a/a/j;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/ftcl/b/a/aC;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/a0;->a:Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/ftcl/b/a/a0;->c:Lcom/jscape/ftcl/b/a/a/j;

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/a/a;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/a0;->c:Lcom/jscape/ftcl/b/a/a/j;

    invoke-virtual {v0, p1}, Lcom/jscape/ftcl/b/a/a/j;->b(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/jscape/ftcl/b/a/ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-interface {p1, p0}, Lcom/jscape/ftcl/b/a/ad;->a(Lcom/jscape/ftcl/b/a/a0;)V

    return-void
.end method

.method public bridge synthetic a(Lcom/jscape/ftcl/b/a/br;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    check-cast p1, Lcom/jscape/ftcl/b/a/ad;

    invoke-virtual {p0, p1}, Lcom/jscape/ftcl/b/a/a0;->a(Lcom/jscape/ftcl/b/a/ad;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/ftcl/b/a/a0;->d:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/ftcl/b/a/a0;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/a0;->c:Lcom/jscape/ftcl/b/a/a/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
