.class public Lcom/jscape/ftcl/b/a/bc;
.super Lcom/jscape/ftcl/b/a/aC;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/ftcl/b/a/aC<",
        "Lcom/jscape/ftcl/b/a/aq;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public final a:Lcom/jscape/ftcl/b/a/a/j;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "\u0007Kp\u0007z5g\u0007Ze>p<g:Z$1x>f1k|:g4q\'Gk$("

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/ftcl/b/a/bc;->c:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x1c

    goto :goto_1

    :cond_1
    const/16 v4, 0x4f

    goto :goto_1

    :cond_2
    const/16 v4, 0xb

    goto :goto_1

    :cond_3
    const/16 v4, 0x54

    goto :goto_1

    :cond_4
    const/16 v4, 0x1a

    goto :goto_1

    :cond_5
    const/16 v4, 0x30

    goto :goto_1

    :cond_6
    const/16 v4, 0x4a

    :goto_1
    const/16 v5, 0x1e

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/jscape/ftcl/b/a/a/j;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/ftcl/b/a/aC;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/bc;->a:Lcom/jscape/ftcl/b/a/a/j;

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/ftcl/b/a/bw;)Lcom/jscape/filetransfer/TransferMode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/a/a;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/bc;->a:Lcom/jscape/ftcl/b/a/a/j;

    invoke-virtual {v0, p1}, Lcom/jscape/ftcl/b/a/a/j;->b(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/String;

    move-result-object p1

    :try_start_0
    invoke-static {p1}, Lcom/jscape/filetransfer/TransferMode;->modeFor(Ljava/lang/String;)Lcom/jscape/filetransfer/TransferMode;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance v0, Lcom/jscape/ftcl/b/a/a/a;

    invoke-direct {v0, p1}, Lcom/jscape/ftcl/b/a/a/a;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public a(Lcom/jscape/ftcl/b/a/aq;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-interface {p1, p0}, Lcom/jscape/ftcl/b/a/aq;->a(Lcom/jscape/ftcl/b/a/bc;)V

    return-void
.end method

.method public bridge synthetic a(Lcom/jscape/ftcl/b/a/br;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    check-cast p1, Lcom/jscape/ftcl/b/a/aq;

    invoke-virtual {p0, p1}, Lcom/jscape/ftcl/b/a/bc;->a(Lcom/jscape/ftcl/b/a/aq;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/ftcl/b/a/bc;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/bc;->a:Lcom/jscape/ftcl/b/a/a/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
