.class public Lcom/jscape/ftcl/b/a/K;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x3c

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x6

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "H @(\u0005I<!+N9\u001bE+r\'Y\'IA,!\"_\'\u000c\u0000}enU&\u0005U5on\u0013-G\u0000}rns1\u0019R=r=_&\u0007\u001ax&kEnXH @(\u0005I<!+N9\u001bE+r\'Y\'IA,!\"_\'\u000c\u0000}enU&\u0005U5on\u0013-S\u0000.`<_(\u000bL=!i\u0013:N\u00001rnX&\u001d\u0000<d-Z(\u001bE</ns1\u0019R=r=_&\u0007\u001ax&kEn<H @(\u0005I<!+N9\u001bE+r\'Y\'IA,!\"_\'\u000c\u0000}enU&\u0005U5on\u0013-G\u0000}rns1\u0019R=r=_&\u0007\u001ax&kEn"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0xd2

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/ftcl/b/a/K;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v13, 0x2

    if-eq v12, v13, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x5e

    goto :goto_2

    :cond_2
    const/16 v12, 0x26

    goto :goto_2

    :cond_3
    const/16 v12, 0x6f

    goto :goto_2

    :cond_4
    const/16 v12, 0x4f

    goto :goto_2

    :cond_5
    const/16 v12, 0x30

    goto :goto_2

    :cond_6
    const/16 v12, 0x48

    goto :goto_2

    :cond_7
    const/4 v12, 0x7

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/jscape/ftcl/b/a/bs;Lcom/jscape/ftcl/b/a/bw;)Lcom/jscape/ftcl/b/a/a/j;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/z;
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/jscape/ftcl/b/a/K;->d(Lcom/jscape/ftcl/b/a/bs;Lcom/jscape/ftcl/b/a/bw;)Lcom/jscape/ftcl/b/a/a/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/ftcl/b/a/a/r;->d()Lcom/jscape/ftcl/b/a/a/j;

    move-result-object v0

    invoke-static {v0, p1, p0}, Lcom/jscape/ftcl/b/a/K;->a(Lcom/jscape/ftcl/b/a/a/f;Lcom/jscape/ftcl/b/a/bw;Lcom/jscape/ftcl/b/a/bs;)V

    return-object v0
.end method

.method private static a(Lcom/jscape/ftcl/b/a/z;)Lcom/jscape/ftcl/b/a/z;
    .locals 0

    return-object p0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/ftcl/b/a/aC;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v0, :cond_0

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    :cond_0
    const/4 v0, 0x2

    if-le v1, v0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_1
    const-string p0, ""

    :cond_2
    :goto_0
    return-object p0
.end method

.method private static a(Lcom/jscape/ftcl/b/a/a/f;Lcom/jscape/ftcl/b/a/bw;Lcom/jscape/ftcl/b/a/bs;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/z;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/ftcl/b/a/a/f;->a(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/jscape/ftcl/b/a/a/b; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/jscape/ftcl/b/a/a/a; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance p1, Lcom/jscape/ftcl/b/a/z;

    sget-object v0, Lcom/jscape/ftcl/b/a/K;->a:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p2, Lcom/jscape/ftcl/b/a/bs;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p2, Lcom/jscape/ftcl/b/a/bs;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0}, Lcom/jscape/ftcl/b/a/a/a;->getMessage()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v2, v1

    const/4 p0, 0x3

    iget-object p2, p2, Lcom/jscape/ftcl/b/a/bs;->f:Ljava/lang/String;

    aput-object p2, v2, p0

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/jscape/ftcl/b/a/z;-><init>(Ljava/lang/String;)V

    throw p1

    :catch_1
    :goto_0
    return-void
.end method

.method private static a(Ljava/util/List;Lcom/jscape/ftcl/b/a/bw;Lcom/jscape/ftcl/b/a/bs;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/jscape/ftcl/b/a/a/f;",
            ">;",
            "Lcom/jscape/ftcl/b/a/bw;",
            "Lcom/jscape/ftcl/b/a/bs;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/z;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/ftcl/b/a/aC;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/ftcl/b/a/a/f;

    if-nez v0, :cond_1

    :try_start_0
    instance-of v2, v1, Lcom/jscape/ftcl/b/a/a/k;
    :try_end_0
    .catch Lcom/jscape/ftcl/b/a/z; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_2

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/ftcl/b/a/K;->a(Lcom/jscape/ftcl/b/a/z;)Lcom/jscape/ftcl/b/a/z;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Lcom/jscape/ftcl/b/a/z; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/ftcl/b/a/K;->a(Lcom/jscape/ftcl/b/a/z;)Lcom/jscape/ftcl/b/a/z;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    check-cast v1, Lcom/jscape/ftcl/b/a/a/k;

    iget-object v1, v1, Lcom/jscape/ftcl/b/a/a/k;->a:Ljava/lang/String;

    :try_start_2
    invoke-virtual {p1, v1}, Lcom/jscape/ftcl/b/a/bw;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    if-eqz v0, :cond_0

    goto :goto_1

    :cond_3
    new-instance p0, Lcom/jscape/ftcl/b/a/z;

    sget-object p1, Lcom/jscape/ftcl/b/a/K;->a:[Ljava/lang/String;

    const/4 v0, 0x1

    aget-object p1, p1, v0

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p2, Lcom/jscape/ftcl/b/a/bs;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    iget v3, p2, Lcom/jscape/ftcl/b/a/bs;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    aput-object v1, v2, v0

    const/4 v0, 0x3

    iget-object p2, p2, Lcom/jscape/ftcl/b/a/bs;->f:Ljava/lang/String;

    aput-object p2, v2, v0

    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/ftcl/b/a/z;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_2
    .catch Lcom/jscape/ftcl/b/a/z; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/ftcl/b/a/K;->a(Lcom/jscape/ftcl/b/a/z;)Lcom/jscape/ftcl/b/a/z;

    move-result-object p0

    throw p0

    :cond_4
    :goto_1
    return-void
.end method

.method public static b(Lcom/jscape/ftcl/b/a/bs;Lcom/jscape/ftcl/b/a/bw;)Lcom/jscape/ftcl/b/a/a/i;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/z;
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/jscape/ftcl/b/a/K;->d(Lcom/jscape/ftcl/b/a/bs;Lcom/jscape/ftcl/b/a/bw;)Lcom/jscape/ftcl/b/a/a/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/ftcl/b/a/a/r;->f()Lcom/jscape/ftcl/b/a/a/i;

    move-result-object v0

    invoke-static {v0, p1, p0}, Lcom/jscape/ftcl/b/a/K;->a(Lcom/jscape/ftcl/b/a/a/f;Lcom/jscape/ftcl/b/a/bw;Lcom/jscape/ftcl/b/a/bs;)V

    return-object v0
.end method

.method public static c(Lcom/jscape/ftcl/b/a/bs;Lcom/jscape/ftcl/b/a/bw;)Lcom/jscape/ftcl/b/a/a/g;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/z;
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/jscape/ftcl/b/a/K;->d(Lcom/jscape/ftcl/b/a/bs;Lcom/jscape/ftcl/b/a/bw;)Lcom/jscape/ftcl/b/a/a/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/ftcl/b/a/a/r;->e()Lcom/jscape/ftcl/b/a/a/g;

    move-result-object v0

    invoke-static {v0, p1, p0}, Lcom/jscape/ftcl/b/a/K;->a(Lcom/jscape/ftcl/b/a/a/f;Lcom/jscape/ftcl/b/a/bw;Lcom/jscape/ftcl/b/a/bs;)V

    return-object v0
.end method

.method private static d(Lcom/jscape/ftcl/b/a/bs;Lcom/jscape/ftcl/b/a/bw;)Lcom/jscape/ftcl/b/a/a/r;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/z;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/ftcl/b/a/bs;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/jscape/ftcl/b/a/K;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/jscape/ftcl/b/a/a/r;->a(Ljava/lang/String;)Lcom/jscape/ftcl/b/a/a/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/ftcl/b/a/a/r;->b()V

    invoke-virtual {v0}, Lcom/jscape/ftcl/b/a/a/r;->c()Ljava/util/List;

    move-result-object v1

    invoke-static {v1, p1, p0}, Lcom/jscape/ftcl/b/a/K;->a(Ljava/util/List;Lcom/jscape/ftcl/b/a/bw;Lcom/jscape/ftcl/b/a/bs;)V
    :try_end_0
    .catch Lcom/jscape/util/f/a; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    new-instance v0, Lcom/jscape/ftcl/b/a/z;

    sget-object v1, Lcom/jscape/ftcl/b/a/K;->a:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/jscape/ftcl/b/a/bs;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v4, 0x1

    iget v5, p0, Lcom/jscape/ftcl/b/a/bs;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {p1}, Lcom/jscape/util/f/a;->getMessage()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v3, v4

    const/4 p1, 0x3

    iget-object p0, p0, Lcom/jscape/ftcl/b/a/bs;->f:Ljava/lang/String;

    aput-object p0, v3, p1

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    new-array p1, v2, [Ljava/lang/Object;

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/jscape/ftcl/b/a/z;-><init>(Ljava/lang/String;)V

    throw v0
.end method
