.class final enum Lcom/jscape/ftcl/b/a/n;
.super Lcom/jscape/ftcl/b/a/b;


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/jscape/ftcl/b/a/b;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/jscape/ftcl/b/a/c;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/ftcl/b/a/bw;Lcom/jscape/ftcl/b/d;)V
    .locals 2

    invoke-static {}, Lcom/jscape/ftcl/b/a/aC;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/n;->v:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/jscape/ftcl/b/a/bw;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/n;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/jscape/ftcl/b/a/bw;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :cond_0
    if-eqz v1, :cond_1

    sget-object p1, Lcom/jscape/filetransfer/Protocol;->SFTP:Lcom/jscape/filetransfer/Protocol;

    invoke-virtual {p2, p1}, Lcom/jscape/ftcl/b/d;->setProtocol(Lcom/jscape/filetransfer/Protocol;)V

    :cond_1
    return-void
.end method

.method public b(Lcom/jscape/ftcl/b/d;Lcom/jscape/ftcl/b/a/bw;)V
    .locals 2

    invoke-virtual {p1}, Lcom/jscape/ftcl/b/d;->getProtocol()Lcom/jscape/filetransfer/Protocol;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/n;->v:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/jscape/ftcl/b/d;->getProtocol()Lcom/jscape/filetransfer/Protocol;

    move-result-object p1

    sget-object v1, Lcom/jscape/filetransfer/Protocol;->SFTP:Lcom/jscape/filetransfer/Protocol;

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p2, v0, p1}, Lcom/jscape/ftcl/b/a/bw;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public c(Ljava/lang/String;)Ljava/lang/Object;
    .locals 0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
