.class public Lcom/jscape/ftcl/b/a/bp;
.super Lcom/jscape/ftcl/b/a/aC;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/ftcl/b/a/aC<",
        "Lcom/jscape/ftcl/b/a/bv;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field public final a:Lcom/jscape/ftcl/b/a/a/j;

.field public final c:Lcom/jscape/ftcl/b/a/a/j;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/16 v2, 0x22

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x28

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "{M(@k\n*Z\\0Jg\u000b\u0017Z\u001d?\\e\u001b\u000bMX\u0001Wz\u001c\u001c]N-@dS\u0018\u0002\u001d Jy\u001a\u0010@\\0Fe\u0000<VM6Jy\u001d\u0010ASy"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x3b

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/ftcl/b/a/bp;->d:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    if-eqz v13, :cond_7

    if-eq v13, v8, :cond_6

    if-eq v13, v0, :cond_5

    const/4 v14, 0x3

    if-eq v13, v14, :cond_4

    const/4 v14, 0x4

    if-eq v13, v14, :cond_3

    const/4 v14, 0x5

    if-eq v13, v14, :cond_2

    const/16 v13, 0x51

    goto :goto_2

    :cond_2
    const/16 v13, 0x46

    goto :goto_2

    :cond_3
    move v13, v2

    goto :goto_2

    :cond_4
    const/4 v13, 0x7

    goto :goto_2

    :cond_5
    const/16 v13, 0x6c

    goto :goto_2

    :cond_6
    const/16 v13, 0x15

    goto :goto_2

    :cond_7
    const/4 v13, 0x6

    :goto_2
    xor-int/2addr v13, v7

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lcom/jscape/ftcl/b/a/a/j;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/ftcl/b/a/aC;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/bp;->a:Lcom/jscape/ftcl/b/a/a/j;

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/bp;->c:Lcom/jscape/ftcl/b/a/a/j;

    return-void
.end method

.method public constructor <init>(Lcom/jscape/ftcl/b/a/a/j;Lcom/jscape/ftcl/b/a/a/j;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/ftcl/b/a/aC;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/bp;->a:Lcom/jscape/ftcl/b/a/a/j;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/ftcl/b/a/bp;->c:Lcom/jscape/ftcl/b/a/a/j;

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/a/a;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/bp;->a:Lcom/jscape/ftcl/b/a/a/j;

    invoke-virtual {v0, p1}, Lcom/jscape/ftcl/b/a/a/j;->b(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Lcom/jscape/ftcl/b/a/br;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    check-cast p1, Lcom/jscape/ftcl/b/a/bv;

    invoke-virtual {p0, p1}, Lcom/jscape/ftcl/b/a/bp;->a(Lcom/jscape/ftcl/b/a/bv;)V

    return-void
.end method

.method public a(Lcom/jscape/ftcl/b/a/bv;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-interface {p1, p0}, Lcom/jscape/ftcl/b/a/bv;->a(Lcom/jscape/ftcl/b/a/bp;)V

    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/bp;->c:Lcom/jscape/ftcl/b/a/a/j;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public b(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/a/a;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/bp;->c:Lcom/jscape/ftcl/b/a/a/j;

    invoke-virtual {v0, p1}, Lcom/jscape/ftcl/b/a/a/j;->b(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/ftcl/b/a/bp;->d:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/ftcl/b/a/bp;->a:Lcom/jscape/ftcl/b/a/a/j;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/bp;->c:Lcom/jscape/ftcl/b/a/a/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
