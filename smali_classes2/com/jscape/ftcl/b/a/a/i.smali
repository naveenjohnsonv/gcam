.class public Lcom/jscape/ftcl/b/a/a/i;
.super Lcom/jscape/ftcl/b/a/a/f;


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/ftcl/b/a/a/j;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x18

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x39

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, ".\u000e\u0018\u0017L\u001d\u0018\"\u0018\u001c\u0000N\u000b\u0019\u000e\u000f\u0002RP\u001a\u000b\u0014\u0005Q+3\u0008\tRN\u0000\u001a\u0015\u0005\u001f\u0001B\u0017\u0004G\u0004\u0003\u0017X\u0016M\u0013@\t\u0004J\u0014\u001f\u0006\u0014\tR_\u0017J\u000e\u000e\u0018\u0017L\u001d\u0018I"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x44

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/ftcl/b/a/a/i;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x53

    goto :goto_2

    :cond_2
    const/16 v12, 0x41

    goto :goto_2

    :cond_3
    const/16 v12, 0x12

    goto :goto_2

    :cond_4
    const/16 v12, 0x4b

    goto :goto_2

    :cond_5
    const/16 v12, 0x55

    goto :goto_2

    :cond_6
    const/16 v12, 0x59

    goto :goto_2

    :cond_7
    const/16 v12, 0x5e

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/jscape/ftcl/b/a/a/f;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/ftcl/b/a/a/f;-><init>()V

    new-instance v0, Lcom/jscape/ftcl/b/a/a/j;

    invoke-direct {v0, p1}, Lcom/jscape/ftcl/b/a/a/j;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/jscape/ftcl/b/a/a/i;->a:Lcom/jscape/ftcl/b/a/a/j;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/a/a;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/ftcl/b/a/a/i;->b(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public b(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/Integer;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/a/a;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/a/i;->a:Lcom/jscape/ftcl/b/a/a/j;

    invoke-virtual {v0, p1}, Lcom/jscape/ftcl/b/a/a/j;->b(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/String;

    move-result-object p1

    :try_start_0
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    new-instance p1, Lcom/jscape/ftcl/b/a/a/a;

    sget-object v0, Lcom/jscape/ftcl/b/a/a/i;->c:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Lcom/jscape/ftcl/b/a/a/a;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/ftcl/b/a/a/i;->c:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/a/i;->a:Lcom/jscape/ftcl/b/a/a/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
