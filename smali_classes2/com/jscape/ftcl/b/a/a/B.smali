.class public Lcom/jscape/ftcl/b/a/a/B;
.super Lcom/jscape/util/f/j;

# interfaces
.implements Lcom/jscape/ftcl/b/a/a/o;
.implements Lcom/jscape/ftcl/b/a/a/c;
.implements Lcom/jscape/ftcl/b/a/a/e;


# instance fields
.field private final b:Ljava/lang/String;

.field private c:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/util/f/j;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/a/B;->b:Ljava/lang/String;

    return-void
.end method

.method private static a(Lcom/jscape/util/f/a;)Lcom/jscape/util/f/a;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public a(Lcom/jscape/ftcl/b/a/a/s;Lcom/jscape/util/f/i;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/ftcl/b/a/a/f;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    :try_start_0
    iget-char v1, p1, Lcom/jscape/ftcl/b/a/a/s;->a:C

    iget-object v2, p0, Lcom/jscape/ftcl/b/a/a/B;->b:Ljava/lang/String;

    iget v3, p0, Lcom/jscape/ftcl/b/a/a/B;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2
    :try_end_0
    .catch Lcom/jscape/util/f/a; {:try_start_0 .. :try_end_0} :catch_3

    if-eqz v0, :cond_0

    if-ne v1, v2, :cond_1

    :try_start_1
    iget v1, p0, Lcom/jscape/ftcl/b/a/a/B;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/jscape/ftcl/b/a/a/B;->c:I

    iget-object v2, p0, Lcom/jscape/ftcl/b/a/a/B;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2
    :try_end_1
    .catch Lcom/jscape/util/f/a; {:try_start_1 .. :try_end_1} :catch_4

    :cond_0
    if-ne v1, v2, :cond_2

    :try_start_2
    new-instance v1, Lcom/jscape/ftcl/b/a/a/v;

    invoke-direct {v1}, Lcom/jscape/ftcl/b/a/a/v;-><init>()V

    invoke-interface {p2, v1}, Lcom/jscape/util/f/i;->a(Lcom/jscape/util/f/g;)V
    :try_end_2
    .catch Lcom/jscape/util/f/a; {:try_start_2 .. :try_end_2} :catch_1

    if-nez v0, :cond_2

    :cond_1
    :try_start_3
    new-instance v0, Lcom/jscape/ftcl/b/a/a/w;

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/a/B;->b:Ljava/lang/String;

    const/4 v2, 0x0

    iget v3, p0, Lcom/jscape/ftcl/b/a/a/B;->c:I

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/ftcl/b/a/a/w;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lcom/jscape/util/f/i;->a(Lcom/jscape/util/f/g;)V

    invoke-interface {p2, p1}, Lcom/jscape/util/f/i;->a(Lcom/jscape/util/f/g;)V

    goto :goto_1

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/a/a/B;->a(Lcom/jscape/util/f/a;)Lcom/jscape/util/f/a;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/util/f/a; {:try_start_3 .. :try_end_3} :catch_0

    :goto_0
    invoke-static {p1}, Lcom/jscape/ftcl/b/a/a/B;->a(Lcom/jscape/util/f/a;)Lcom/jscape/util/f/a;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    :try_start_4
    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_3

    const/4 p1, 0x5

    new-array p1, p1, [Lcom/jscape/util/aq;

    invoke-static {p1}, Lcom/jscape/ftcl/b/a/a/f;->b([Lcom/jscape/util/aq;)V
    :try_end_4
    .catch Lcom/jscape/util/f/a; {:try_start_4 .. :try_end_4} :catch_2

    :cond_3
    return-void

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/a/a/B;->a(Lcom/jscape/util/f/a;)Lcom/jscape/util/f/a;

    move-result-object p1

    throw p1

    :catch_3
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/ftcl/b/a/a/B;->a(Lcom/jscape/util/f/a;)Lcom/jscape/util/f/a;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/util/f/a; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/a/a/B;->a(Lcom/jscape/util/f/a;)Lcom/jscape/util/f/a;

    move-result-object p1

    throw p1
.end method

.method public a(Lcom/jscape/ftcl/b/a/a/u;Lcom/jscape/util/f/i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    new-instance p1, Lcom/jscape/ftcl/b/a/a/w;

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/a/B;->b:Ljava/lang/String;

    iget v1, p0, Lcom/jscape/ftcl/b/a/a/B;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/jscape/ftcl/b/a/a/w;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/jscape/util/f/i;->a(Lcom/jscape/util/f/g;)V

    return-void
.end method

.method public a(Lcom/jscape/ftcl/b/a/a/y;Lcom/jscape/util/f/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    const/4 p1, 0x0

    iput p1, p0, Lcom/jscape/ftcl/b/a/a/B;->c:I

    return-void
.end method
