.class public Lcom/jscape/ftcl/b/a/a/k;
.super Lcom/jscape/ftcl/b/a/a/f;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "{\u0006w65E\u0002H\"}/&B\u001d^\u000ej1t\\\u0018L\u0015l>6K\u000b\u0010@"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/ftcl/b/a/a/k;->c:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/4 v5, 0x3

    if-eqz v4, :cond_5

    const/4 v6, 0x1

    if-eq v4, v6, :cond_6

    const/4 v6, 0x2

    if-eq v4, v6, :cond_4

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v5, 0xa

    goto :goto_1

    :cond_1
    const/16 v5, 0x43

    goto :goto_1

    :cond_2
    const/16 v5, 0x30

    goto :goto_1

    :cond_3
    const/16 v5, 0x3b

    goto :goto_1

    :cond_4
    const/16 v5, 0x61

    goto :goto_1

    :cond_5
    const/16 v5, 0x49

    :cond_6
    :goto_1
    const/16 v4, 0x64

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/ftcl/b/a/a/f;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/a/k;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/a/a;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/ftcl/b/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/jscape/ftcl/b/a/bw;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    new-instance p1, Lcom/jscape/ftcl/b/a/a/a;

    invoke-direct {p1}, Lcom/jscape/ftcl/b/a/a/a;-><init>()V

    throw p1

    :catch_1
    new-instance p1, Lcom/jscape/ftcl/b/a/a/b;

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/a/k;->a:Ljava/lang/String;

    invoke-direct {p1, v0}, Lcom/jscape/ftcl/b/a/a/b;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/ftcl/b/a/a/k;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
