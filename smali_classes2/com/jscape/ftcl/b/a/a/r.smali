.class public Lcom/jscape/ftcl/b/a/a/r;
.super Lcom/jscape/util/f/d;


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/jscape/ftcl/b/a/a/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "_&"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/ftcl/b/a/a/r;->f:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v5, 0x12

    if-eqz v4, :cond_6

    const/4 v6, 0x1

    if-eq v4, v6, :cond_5

    const/4 v6, 0x2

    if-eq v4, v6, :cond_4

    const/4 v6, 0x3

    if-eq v4, v6, :cond_3

    const/4 v6, 0x4

    if-eq v4, v6, :cond_2

    const/4 v6, 0x5

    if-eq v4, v6, :cond_1

    const/16 v4, 0x4a

    goto :goto_1

    :cond_1
    move v4, v5

    goto :goto_1

    :cond_2
    const/16 v4, 0x40

    goto :goto_1

    :cond_3
    const/16 v4, 0x5d

    goto :goto_1

    :cond_4
    const/16 v4, 0x28

    goto :goto_1

    :cond_5
    const/16 v4, 0x4f

    goto :goto_1

    :cond_6
    const/16 v4, 0x69

    :goto_1
    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Lcom/jscape/util/f/l;Lcom/jscape/util/f/i;Lcom/jscape/util/f/j;Lcom/jscape/util/f/j;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/f/l;",
            "Lcom/jscape/util/f/i;",
            "Lcom/jscape/util/f/j;",
            "Lcom/jscape/util/f/j;",
            "Ljava/util/List<",
            "Lcom/jscape/ftcl/b/a/a/f;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/jscape/util/f/d;-><init>(Lcom/jscape/util/f/l;Lcom/jscape/util/f/i;Lcom/jscape/util/f/j;Lcom/jscape/util/f/j;)V

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/ftcl/b/a/a/r;->e:Ljava/util/List;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/ftcl/b/a/a/r;
    .locals 12

    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    new-instance v3, Lcom/jscape/ftcl/b/a/a/A;

    const/16 v0, 0x24

    invoke-direct {v3, v0, v5}, Lcom/jscape/ftcl/b/a/a/A;-><init>(CLjava/util/List;)V

    new-instance v0, Lcom/jscape/ftcl/b/a/a/C;

    const/16 v1, 0x7d

    invoke-direct {v0, v1, v5}, Lcom/jscape/ftcl/b/a/a/C;-><init>(CLjava/util/List;)V

    new-instance v1, Lcom/jscape/ftcl/b/a/a/B;

    sget-object v2, Lcom/jscape/ftcl/b/a/a/r;->f:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/jscape/ftcl/b/a/a/B;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/jscape/ftcl/b/a/a/B;

    const-string v4, "}"

    invoke-direct {v2, v4}, Lcom/jscape/ftcl/b/a/a/B;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/ftcl/b/a/a/f;->b()[Lcom/jscape/util/aq;

    move-result-object v6

    sget-object v4, Lcom/jscape/util/f/j;->a:Lcom/jscape/util/f/j;

    new-instance v7, Lcom/jscape/util/f/e;

    const/16 v8, 0x12

    new-array v8, v8, [Lcom/jscape/util/f/f;

    new-instance v9, Lcom/jscape/util/f/f;

    const-class v10, Lcom/jscape/ftcl/b/a/a/s;

    invoke-direct {v9, v3, v10, v3}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/4 v10, 0x0

    aput-object v9, v8, v10

    new-instance v9, Lcom/jscape/util/f/f;

    const-class v10, Lcom/jscape/ftcl/b/a/a/u;

    invoke-direct {v9, v3, v10, v3}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/4 v10, 0x1

    aput-object v9, v8, v10

    new-instance v9, Lcom/jscape/util/f/f;

    const-class v10, Lcom/jscape/ftcl/b/a/a/t;

    invoke-direct {v9, v3, v10, v4}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/4 v10, 0x2

    aput-object v9, v8, v10

    new-instance v9, Lcom/jscape/util/f/f;

    const-class v11, Lcom/jscape/ftcl/b/a/a/y;

    invoke-direct {v9, v3, v11, v1}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/4 v11, 0x3

    aput-object v9, v8, v11

    new-instance v9, Lcom/jscape/util/f/f;

    const-class v11, Lcom/jscape/ftcl/b/a/a/z;

    invoke-direct {v9, v3, v11, v0}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/4 v11, 0x4

    aput-object v9, v8, v11

    new-instance v9, Lcom/jscape/util/f/f;

    const-class v11, Lcom/jscape/ftcl/b/a/a/s;

    invoke-direct {v9, v1, v11, v1}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/4 v11, 0x5

    aput-object v9, v8, v11

    new-instance v9, Lcom/jscape/util/f/f;

    const-class v11, Lcom/jscape/ftcl/b/a/a/u;

    invoke-direct {v9, v1, v11, v1}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/4 v11, 0x6

    aput-object v9, v8, v11

    new-instance v9, Lcom/jscape/util/f/f;

    const-class v11, Lcom/jscape/ftcl/b/a/a/v;

    invoke-direct {v9, v1, v11, v3}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/4 v11, 0x7

    aput-object v9, v8, v11

    new-instance v9, Lcom/jscape/util/f/f;

    const-class v11, Lcom/jscape/ftcl/b/a/a/w;

    invoke-direct {v9, v1, v11, v3}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/16 v1, 0x8

    aput-object v9, v8, v1

    new-instance v1, Lcom/jscape/util/f/f;

    const-class v9, Lcom/jscape/ftcl/b/a/a/s;

    invoke-direct {v1, v0, v9, v0}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/16 v9, 0x9

    aput-object v1, v8, v9

    new-instance v1, Lcom/jscape/util/f/f;

    const-class v9, Lcom/jscape/ftcl/b/a/a/u;

    invoke-direct {v1, v0, v9, v0}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/16 v9, 0xa

    aput-object v1, v8, v9

    new-instance v1, Lcom/jscape/util/f/f;

    const-class v9, Lcom/jscape/ftcl/b/a/a/t;

    invoke-direct {v1, v0, v9, v4}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/16 v9, 0xb

    aput-object v1, v8, v9

    new-instance v1, Lcom/jscape/util/f/f;

    const-class v9, Lcom/jscape/ftcl/b/a/a/x;

    invoke-direct {v1, v0, v9, v3}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/16 v9, 0xc

    aput-object v1, v8, v9

    new-instance v1, Lcom/jscape/util/f/f;

    const-class v9, Lcom/jscape/ftcl/b/a/a/y;

    invoke-direct {v1, v0, v9, v2}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/16 v9, 0xd

    aput-object v1, v8, v9

    new-instance v1, Lcom/jscape/util/f/f;

    const-class v9, Lcom/jscape/ftcl/b/a/a/s;

    invoke-direct {v1, v2, v9, v2}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/16 v9, 0xe

    aput-object v1, v8, v9

    new-instance v1, Lcom/jscape/util/f/f;

    const-class v9, Lcom/jscape/ftcl/b/a/a/u;

    invoke-direct {v1, v2, v9, v2}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/16 v9, 0xf

    aput-object v1, v8, v9

    new-instance v1, Lcom/jscape/util/f/f;

    const-class v9, Lcom/jscape/ftcl/b/a/a/v;

    invoke-direct {v1, v2, v9, v0}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/16 v9, 0x10

    aput-object v1, v8, v9

    new-instance v1, Lcom/jscape/util/f/f;

    const-class v9, Lcom/jscape/ftcl/b/a/a/w;

    invoke-direct {v1, v2, v9, v0}, Lcom/jscape/util/f/f;-><init>(Lcom/jscape/util/f/j;Ljava/lang/Class;Lcom/jscape/util/f/j;)V

    const/16 v0, 0x11

    aput-object v1, v8, v0

    invoke-direct {v7, v8}, Lcom/jscape/util/f/e;-><init>([Lcom/jscape/util/f/f;)V

    new-instance v2, Lcom/jscape/ftcl/b/a/a/q;

    invoke-direct {v2, p0}, Lcom/jscape/ftcl/b/a/a/q;-><init>(Ljava/lang/String;)V

    new-instance p0, Lcom/jscape/ftcl/b/a/a/r;

    move-object v0, p0

    move-object v1, v7

    invoke-direct/range {v0 .. v5}, Lcom/jscape/ftcl/b/a/a/r;-><init>(Lcom/jscape/util/f/l;Lcom/jscape/util/f/i;Lcom/jscape/util/f/j;Lcom/jscape/util/f/j;Ljava/util/List;)V

    if-nez v6, :cond_0

    new-array v0, v10, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object p0
.end method


# virtual methods
.method public b()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/ftcl/b/a/a/r;->a()V

    return-void
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/jscape/ftcl/b/a/a/f;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/a/r;->e:Ljava/util/List;

    return-object v0
.end method

.method public d()Lcom/jscape/ftcl/b/a/a/j;
    .locals 2

    new-instance v0, Lcom/jscape/ftcl/b/a/a/j;

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/a/r;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Lcom/jscape/ftcl/b/a/a/j;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public e()Lcom/jscape/ftcl/b/a/a/g;
    .locals 2

    new-instance v0, Lcom/jscape/ftcl/b/a/a/g;

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/a/r;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Lcom/jscape/ftcl/b/a/a/g;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public f()Lcom/jscape/ftcl/b/a/a/i;
    .locals 2

    new-instance v0, Lcom/jscape/ftcl/b/a/a/i;

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/a/r;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Lcom/jscape/ftcl/b/a/a/i;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method
