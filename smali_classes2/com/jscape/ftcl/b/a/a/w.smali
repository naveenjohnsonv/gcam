.class public Lcom/jscape/ftcl/b/a/a/w;
.super Lcom/jscape/util/f/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/util/f/g<",
        "Lcom/jscape/ftcl/b/a/a/m;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "\u001d\u0005\\\"\u0017&T\u0016\u0004_-\u001f\nF1\u0005^c\u0000+Q \n\u0017d"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/ftcl/b/a/a/w;->c:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x69

    goto :goto_1

    :cond_1
    const/16 v4, 0x16

    goto :goto_1

    :cond_2
    const/16 v4, 0x22

    goto :goto_1

    :cond_3
    const/16 v4, 0x1a

    goto :goto_1

    :cond_4
    const/16 v4, 0x73

    goto :goto_1

    :cond_5
    const/16 v4, 0x32

    goto :goto_1

    :cond_6
    const/16 v4, 0xd

    :goto_1
    const/16 v5, 0x59

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/util/f/g;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/a/w;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/ftcl/b/a/a/m;Lcom/jscape/util/f/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/ftcl/b/a/a/m;->a(Lcom/jscape/ftcl/b/a/a/w;Lcom/jscape/util/f/i;)V

    return-void
.end method

.method public bridge synthetic a(Lcom/jscape/util/f/h;Lcom/jscape/util/f/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    check-cast p1, Lcom/jscape/ftcl/b/a/a/m;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/ftcl/b/a/a/w;->a(Lcom/jscape/ftcl/b/a/a/m;Lcom/jscape/util/f/i;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/ftcl/b/a/a/w;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/a/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
