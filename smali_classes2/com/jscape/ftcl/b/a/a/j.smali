.class public Lcom/jscape/ftcl/b/a/a/j;
.super Lcom/jscape/ftcl/b/a/a/f;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Lcom/jscape/ftcl/b/a/a/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "\u0019Q\u0012BRY\"2U\u0012NOM\u000e%K@PYF\u00178@\u0013XUQ\t9\u0018"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/ftcl/b/a/a/j;->c:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x1c

    goto :goto_1

    :cond_1
    const/16 v4, 0x45

    goto :goto_1

    :cond_2
    const/16 v4, 0x47

    goto :goto_1

    :cond_3
    const/16 v4, 0x50

    goto :goto_1

    :cond_4
    const/16 v4, 0x1b

    goto :goto_1

    :cond_5
    const/16 v4, 0x5e

    goto :goto_1

    :cond_6
    const/16 v4, 0x31

    :goto_1
    const/16 v5, 0x7b

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/jscape/ftcl/b/a/a/f;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/ftcl/b/a/a/f;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/a/j;->a:Ljava/util/Collection;

    return-void
.end method

.method public varargs constructor <init>([Lcom/jscape/ftcl/b/a/a/f;)V
    .locals 0

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/ftcl/b/a/a/j;-><init>(Ljava/util/Collection;)V

    return-void
.end method

.method private static a(Lcom/jscape/ftcl/b/a/a/a;)Lcom/jscape/ftcl/b/a/a/a;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public bridge synthetic a(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/a/a;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/ftcl/b/a/a/j;->b(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public b(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/a/a;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/jscape/ftcl/b/a/a/f;->b()[Lcom/jscape/util/aq;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/ftcl/b/a/a/j;->a:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/ftcl/b/a/a/f;

    :try_start_0
    invoke-virtual {v3, p1}, Lcom/jscape/ftcl/b/a/a/f;->a(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Lcom/jscape/ftcl/b/a/a/a; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_1

    if-nez v1, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/a/a/j;->a(Lcom/jscape/ftcl/b/a/a/a;)Lcom/jscape/ftcl/b/a/a/a;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/ftcl/b/a/a/j;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/a/j;->a:Ljava/util/Collection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
