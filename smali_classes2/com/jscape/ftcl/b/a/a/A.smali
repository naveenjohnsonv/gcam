.class public Lcom/jscape/ftcl/b/a/a/A;
.super Lcom/jscape/util/f/j;

# interfaces
.implements Lcom/jscape/ftcl/b/a/a/c;
.implements Lcom/jscape/ftcl/b/a/a/n;
.implements Lcom/jscape/ftcl/b/a/a/l;
.implements Lcom/jscape/ftcl/b/a/a/m;
.implements Lcom/jscape/ftcl/b/a/a/e;


# instance fields
.field private final b:C

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/jscape/ftcl/b/a/a/f;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(CLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(C",
            "Ljava/util/List<",
            "Lcom/jscape/ftcl/b/a/a/f;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/util/f/j;-><init>()V

    iput-char p1, p0, Lcom/jscape/ftcl/b/a/a/A;->b:C

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/ftcl/b/a/a/A;->c:Ljava/util/List;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/a/A;->d:Ljava/lang/StringBuilder;

    return-void
.end method

.method private static a(Lcom/jscape/util/f/a;)Lcom/jscape/util/f/a;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 2

    invoke-static {}, Lcom/jscape/ftcl/b/a/a/f;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/a/A;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Lcom/jscape/ftcl/b/a/a/h;

    invoke-direct {v0, v1}, Lcom/jscape/ftcl/b/a/a/h;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/a/A;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/jscape/ftcl/b/a/a/A;->d:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/ftcl/b/a/a/s;Lcom/jscape/util/f/i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/ftcl/b/a/a/f;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-char v1, p1, Lcom/jscape/ftcl/b/a/a/s;->a:C

    if-eqz v0, :cond_0

    :try_start_0
    iget-char v2, p0, Lcom/jscape/ftcl/b/a/a/A;->b:C
    :try_end_0
    .catch Lcom/jscape/util/f/a; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v1, v2, :cond_1

    :try_start_1
    new-instance v2, Lcom/jscape/ftcl/b/a/a/y;

    invoke-direct {v2}, Lcom/jscape/ftcl/b/a/a/y;-><init>()V

    invoke-interface {p2, v2}, Lcom/jscape/util/f/i;->a(Lcom/jscape/util/f/g;)V

    invoke-interface {p2, p1}, Lcom/jscape/util/f/i;->a(Lcom/jscape/util/f/g;)V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/a/a/A;->a(Lcom/jscape/util/f/a;)Lcom/jscape/util/f/a;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/util/f/a; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/a/a/A;->a(Lcom/jscape/util/f/a;)Lcom/jscape/util/f/a;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    if-nez v0, :cond_2

    :cond_1
    :try_start_2
    iget-object p1, p0, Lcom/jscape/ftcl/b/a/a/A;->d:Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Lcom/jscape/util/f/a; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/a/a/A;->a(Lcom/jscape/util/f/a;)Lcom/jscape/util/f/a;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    return-void
.end method

.method public a(Lcom/jscape/ftcl/b/a/a/u;Lcom/jscape/util/f/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/ftcl/b/a/a/A;->a()V

    new-instance p1, Lcom/jscape/ftcl/b/a/a/t;

    invoke-direct {p1}, Lcom/jscape/ftcl/b/a/a/t;-><init>()V

    invoke-interface {p2, p1}, Lcom/jscape/util/f/i;->a(Lcom/jscape/util/f/g;)V

    return-void
.end method

.method public a(Lcom/jscape/ftcl/b/a/a/v;Lcom/jscape/util/f/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/ftcl/b/a/a/A;->a()V

    new-instance p1, Lcom/jscape/ftcl/b/a/a/z;

    invoke-direct {p1}, Lcom/jscape/ftcl/b/a/a/z;-><init>()V

    invoke-interface {p2, p1}, Lcom/jscape/util/f/i;->a(Lcom/jscape/util/f/g;)V

    return-void
.end method

.method public a(Lcom/jscape/ftcl/b/a/a/w;Lcom/jscape/util/f/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    iget-object p2, p0, Lcom/jscape/ftcl/b/a/a/A;->d:Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/jscape/ftcl/b/a/a/w;->a:Ljava/lang/String;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method public a(Lcom/jscape/ftcl/b/a/a/x;Lcom/jscape/util/f/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    return-void
.end method
