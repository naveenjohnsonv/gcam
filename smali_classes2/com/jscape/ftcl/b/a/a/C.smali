.class public Lcom/jscape/ftcl/b/a/a/C;
.super Lcom/jscape/util/f/j;

# interfaces
.implements Lcom/jscape/ftcl/b/a/a/c;
.implements Lcom/jscape/ftcl/b/a/a/p;
.implements Lcom/jscape/ftcl/b/a/a/l;
.implements Lcom/jscape/ftcl/b/a/a/m;
.implements Lcom/jscape/ftcl/b/a/a/e;


# static fields
.field private static final e:[Ljava/lang/String;


# instance fields
.field private final b:C

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/jscape/ftcl/b/a/a/f;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x2d

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x5a

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\u0018\u001bjg\u001f\u0016oq\u0011}r\u0012_m>\u0007qg\u0007E+\'\u0014no\u0012\u001dg4Uko\u0007\u0017d$\u0001<gS\u0011j<\u001022\u0018\u001bjg\u001f\u0016oq\u0011}r\u0012_m>\u0007qg\u0007E+\'\u0014no\u0012\u001dg4Uri\u0007_{#\u001alc\u0001\u0013rq\u0016pi\u0000\u001ao\u007f"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x60

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/ftcl/b/a/a/C;->e:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x51

    goto :goto_2

    :cond_2
    const/16 v12, 0x25

    goto :goto_2

    :cond_3
    const/16 v12, 0x29

    goto :goto_2

    :cond_4
    const/16 v12, 0x5c

    goto :goto_2

    :cond_5
    const/16 v12, 0x46

    goto :goto_2

    :cond_6
    const/16 v12, 0x2f

    goto :goto_2

    :cond_7
    const/16 v12, 0xb

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(CLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(C",
            "Ljava/util/List<",
            "Lcom/jscape/ftcl/b/a/a/f;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/util/f/j;-><init>()V

    iput-char p1, p0, Lcom/jscape/ftcl/b/a/a/C;->b:C

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/ftcl/b/a/a/C;->c:Ljava/util/List;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/a/C;->d:Ljava/lang/StringBuilder;

    return-void
.end method

.method private static a(Lcom/jscape/util/f/a;)Lcom/jscape/util/f/a;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/a/C;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1
    :try_end_0
    .catch Lcom/jscape/util/f/a; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    new-instance v1, Lcom/jscape/ftcl/b/a/a/k;

    invoke-direct {v1, v0}, Lcom/jscape/ftcl/b/a/a/k;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/a/C;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/a/C;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lcom/jscape/util/f/a;

    sget-object v1, Lcom/jscape/ftcl/b/a/a/C;->e:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/util/f/a;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/jscape/util/f/a; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/ftcl/b/a/a/C;->a(Lcom/jscape/util/f/a;)Lcom/jscape/util/f/a;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public a(Lcom/jscape/ftcl/b/a/a/s;Lcom/jscape/util/f/i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/ftcl/b/a/a/f;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-char v1, p1, Lcom/jscape/ftcl/b/a/a/s;->a:C

    if-eqz v0, :cond_0

    :try_start_0
    iget-char v2, p0, Lcom/jscape/ftcl/b/a/a/C;->b:C
    :try_end_0
    .catch Lcom/jscape/util/f/a; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v1, v2, :cond_1

    :try_start_1
    new-instance v2, Lcom/jscape/ftcl/b/a/a/y;

    invoke-direct {v2}, Lcom/jscape/ftcl/b/a/a/y;-><init>()V

    invoke-interface {p2, v2}, Lcom/jscape/util/f/i;->a(Lcom/jscape/util/f/g;)V

    invoke-interface {p2, p1}, Lcom/jscape/util/f/i;->a(Lcom/jscape/util/f/g;)V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/a/a/C;->a(Lcom/jscape/util/f/a;)Lcom/jscape/util/f/a;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/util/f/a; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/a/a/C;->a(Lcom/jscape/util/f/a;)Lcom/jscape/util/f/a;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    if-nez v0, :cond_2

    :cond_1
    :try_start_2
    iget-object p1, p0, Lcom/jscape/ftcl/b/a/a/C;->d:Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Lcom/jscape/util/f/a; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/a/a/C;->a(Lcom/jscape/util/f/a;)Lcom/jscape/util/f/a;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    return-void
.end method

.method public a(Lcom/jscape/ftcl/b/a/a/u;Lcom/jscape/util/f/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    new-instance p1, Lcom/jscape/util/f/a;

    sget-object p2, Lcom/jscape/ftcl/b/a/a/C;->e:[Ljava/lang/String;

    const/4 v0, 0x1

    aget-object p2, p2, v0

    invoke-direct {p1, p2}, Lcom/jscape/util/f/a;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Lcom/jscape/ftcl/b/a/a/v;Lcom/jscape/util/f/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/ftcl/b/a/a/C;->a()V

    new-instance p1, Lcom/jscape/ftcl/b/a/a/x;

    invoke-direct {p1}, Lcom/jscape/ftcl/b/a/a/x;-><init>()V

    invoke-interface {p2, p1}, Lcom/jscape/util/f/i;->a(Lcom/jscape/util/f/g;)V

    return-void
.end method

.method public a(Lcom/jscape/ftcl/b/a/a/w;Lcom/jscape/util/f/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    iget-object p2, p0, Lcom/jscape/ftcl/b/a/a/C;->d:Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/jscape/ftcl/b/a/a/w;->a:Ljava/lang/String;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method public a(Lcom/jscape/ftcl/b/a/a/z;Lcom/jscape/util/f/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    return-void
.end method
