.class public Lcom/jscape/ftcl/b/a/a/g;
.super Lcom/jscape/ftcl/b/a/a/f;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/ftcl/b/a/a/j;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const-string v0, "X\u007f\u0010\u0017\u000b\t3_h\u000f\t\u000b\u001b.s\u007f\u0011[\u0015\n<iuB"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/ftcl/b/a/a/g;->c:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/4 v5, 0x4

    const/4 v6, 0x2

    if-eqz v4, :cond_5

    const/4 v7, 0x1

    if-eq v4, v7, :cond_4

    if-eq v4, v6, :cond_3

    const/4 v7, 0x3

    if-eq v4, v7, :cond_2

    if-eq v4, v5, :cond_1

    const/4 v6, 0x5

    if-eq v4, v6, :cond_6

    const/16 v5, 0x31

    goto :goto_1

    :cond_1
    move v5, v6

    goto :goto_1

    :cond_2
    const/16 v5, 0x17

    goto :goto_1

    :cond_3
    const/16 v5, 0x13

    goto :goto_1

    :cond_4
    const/16 v5, 0x7c

    goto :goto_1

    :cond_5
    const/16 v5, 0x76

    :cond_6
    :goto_1
    const/16 v4, 0x6c

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/jscape/ftcl/b/a/a/f;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/ftcl/b/a/a/f;-><init>()V

    new-instance v0, Lcom/jscape/ftcl/b/a/a/j;

    invoke-direct {v0, p1}, Lcom/jscape/ftcl/b/a/a/j;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/jscape/ftcl/b/a/a/g;->a:Lcom/jscape/ftcl/b/a/a/j;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/a/a;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/ftcl/b/a/a/g;->b(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public b(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/Boolean;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/a/a;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/a/g;->a:Lcom/jscape/ftcl/b/a/a/j;

    invoke-virtual {v0, p1}, Lcom/jscape/ftcl/b/a/a/j;->b(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/ftcl/b/a/a/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/a/g;->a:Lcom/jscape/ftcl/b/a/a/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
