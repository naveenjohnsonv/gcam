.class public Lcom/jscape/ftcl/b/a/a7;
.super Lcom/jscape/ftcl/b/a/aC;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/ftcl/b/a/aC<",
        "Lcom/jscape/ftcl/b/a/ak;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public final a:Lcom/jscape/ftcl/b/a/a/i;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "Zttu\u0016B\u0010M~wZ\u001cY\u0001mCa@\u0015e\u0014heeY\u0015X\u0014)jrU\u0004S%qarQ\u0003E\tf\u007f="

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/ftcl/b/a/a7;->c:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v5, 0xf

    if-eqz v4, :cond_6

    const/4 v6, 0x1

    if-eq v4, v6, :cond_5

    const/4 v6, 0x2

    if-eq v4, v6, :cond_4

    const/4 v6, 0x3

    if-eq v4, v6, :cond_3

    const/4 v6, 0x4

    if-eq v4, v6, :cond_2

    const/4 v6, 0x5

    if-eq v4, v6, :cond_1

    const/16 v4, 0x6f

    goto :goto_1

    :cond_1
    const/16 v4, 0x39

    goto :goto_1

    :cond_2
    const/16 v4, 0x7f

    goto :goto_1

    :cond_3
    const/16 v4, 0x3b

    goto :goto_1

    :cond_4
    move v4, v5

    goto :goto_1

    :cond_5
    const/16 v4, 0x1e

    goto :goto_1

    :cond_6
    const/4 v4, 0x6

    :goto_1
    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/jscape/ftcl/b/a/a/i;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/ftcl/b/a/aC;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/a7;->a:Lcom/jscape/ftcl/b/a/a/i;

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/ftcl/b/a/bw;)Lcom/jscape/util/A;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/ftcl/b/a/a/a;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/a7;->a:Lcom/jscape/ftcl/b/a/a/i;

    invoke-virtual {v0, p1}, Lcom/jscape/ftcl/b/a/a/i;->b(Lcom/jscape/ftcl/b/a/bw;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v0, p1

    invoke-static {v0, v1}, Lcom/jscape/util/A;->b(J)Lcom/jscape/util/A;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/jscape/ftcl/b/a/ak;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-interface {p1, p0}, Lcom/jscape/ftcl/b/a/ak;->a(Lcom/jscape/ftcl/b/a/a7;)V

    return-void
.end method

.method public bridge synthetic a(Lcom/jscape/ftcl/b/a/br;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    check-cast p1, Lcom/jscape/ftcl/b/a/ak;

    invoke-virtual {p0, p1}, Lcom/jscape/ftcl/b/a/a7;->a(Lcom/jscape/ftcl/b/a/ak;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/ftcl/b/a/a7;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/a7;->a:Lcom/jscape/ftcl/b/a/a/i;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
