.class public Lcom/jscape/ftcl/b/a/aB;
.super Ljava/lang/Object;


# static fields
.field public static final a:Z


# instance fields
.field b:I

.field c:I

.field d:I

.field public e:I

.field protected f:[I

.field protected g:[I

.field protected h:I

.field protected i:I

.field protected j:Z

.field protected k:Z

.field protected l:Ljava/io/Reader;

.field protected m:[C

.field protected n:I

.field protected o:I

.field protected p:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2

    const/4 v0, 0x1

    const/16 v1, 0x1000

    invoke-direct {p0, p1, v0, v0, v1}, Lcom/jscape/ftcl/b/a/aB;-><init>(Ljava/io/InputStream;III)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;II)V
    .locals 1

    const/16 v0, 0x1000

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/jscape/ftcl/b/a/aB;-><init>(Ljava/io/InputStream;III)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;III)V
    .locals 1

    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/jscape/ftcl/b/a/aB;-><init>(Ljava/io/Reader;III)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/16 v5, 0x1000

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/jscape/ftcl/b/a/aB;-><init>(Ljava/io/InputStream;Ljava/lang/String;III)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;II)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const/16 v5, 0x1000

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/ftcl/b/a/aB;-><init>(Ljava/io/InputStream;Ljava/lang/String;III)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;III)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    if-nez p2, :cond_0

    new-instance p2, Ljava/io/InputStreamReader;

    invoke-direct {p2, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p1, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object p2, v0

    :goto_0
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/jscape/ftcl/b/a/aB;-><init>(Ljava/io/Reader;III)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 2

    const/4 v0, 0x1

    const/16 v1, 0x1000

    invoke-direct {p0, p1, v0, v0, v1}, Lcom/jscape/ftcl/b/a/aB;-><init>(Ljava/io/Reader;III)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;II)V
    .locals 1

    const/16 v0, 0x1000

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/jscape/ftcl/b/a/aB;-><init>(Ljava/io/Reader;III)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;III)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/ftcl/b/a/aB;->h:I

    const/4 v1, 0x1

    iput v1, p0, Lcom/jscape/ftcl/b/a/aB;->i:I

    iput-boolean v0, p0, Lcom/jscape/ftcl/b/a/aB;->j:Z

    iput-boolean v0, p0, Lcom/jscape/ftcl/b/a/aB;->k:Z

    iput v0, p0, Lcom/jscape/ftcl/b/a/aB;->n:I

    iput v0, p0, Lcom/jscape/ftcl/b/a/aB;->o:I

    const/16 v0, 0x8

    iput v0, p0, Lcom/jscape/ftcl/b/a/aB;->p:I

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->l:Ljava/io/Reader;

    iput p2, p0, Lcom/jscape/ftcl/b/a/aB;->i:I

    sub-int/2addr p3, v1

    iput p3, p0, Lcom/jscape/ftcl/b/a/aB;->h:I

    iput p4, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    iput p4, p0, Lcom/jscape/ftcl/b/a/aB;->c:I

    new-array p1, p4, [C

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    new-array p1, p4, [I

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    new-array p1, p4, [I

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->g:[I

    return-void
.end method

.method private static a(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/ftcl/b/a/aC;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->n:I

    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->c:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_9

    const/4 v3, 0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    if-nez v0, :cond_9

    if-ne v1, v2, :cond_8

    :try_start_1
    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->c:I

    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->b:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_b

    const/16 v6, 0x800

    if-nez v0, :cond_4

    if-ne v1, v2, :cond_3

    :try_start_2
    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->d:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_d

    if-nez v0, :cond_1

    if-le v1, v6, :cond_0

    :try_start_3
    iput v5, p0, Lcom/jscape/ftcl/b/a/aB;->n:I

    iput v5, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    iput v1, p0, Lcom/jscape/ftcl/b/a/aB;->c:I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_f

    if-eqz v0, :cond_8

    :cond_0
    if-nez v0, :cond_2

    :try_start_4
    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->d:I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_10

    :cond_1
    if-gez v1, :cond_2

    :try_start_5
    iput v5, p0, Lcom/jscape/ftcl/b/a/aB;->n:I

    iput v5, p0, Lcom/jscape/ftcl/b/a/aB;->e:I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    if-eqz v0, :cond_8

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_2
    :goto_0
    :try_start_6
    invoke-virtual {p0, v5}, Lcom/jscape/ftcl/b/a/aB;->a(Z)V

    if-eqz v0, :cond_8

    :cond_3
    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->c:I

    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->d:I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_4
    :goto_1
    if-nez v0, :cond_6

    if-le v1, v2, :cond_5

    :try_start_7
    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    iput v1, p0, Lcom/jscape/ftcl/b/a/aB;->c:I
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    if-eqz v0, :cond_8

    goto :goto_2

    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_5
    :goto_2
    if-nez v0, :cond_7

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->c:I
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    sub-int/2addr v1, v2

    goto :goto_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_6
    move v6, v2

    :goto_3
    if-ge v1, v6, :cond_7

    :try_start_9
    invoke-virtual {p0, v3}, Lcom/jscape/ftcl/b/a/aB;->a(Z)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    if-eqz v0, :cond_8

    goto :goto_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_7
    :goto_4
    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    iput v1, p0, Lcom/jscape/ftcl/b/a/aB;->c:I

    :cond_8
    :try_start_a
    iget-object v1, p0, Lcom/jscape/ftcl/b/a/aB;->l:Ljava/io/Reader;

    iget-object v2, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    iget v6, p0, Lcom/jscape/ftcl/b/a/aB;->n:I

    iget v7, p0, Lcom/jscape/ftcl/b/a/aB;->c:I

    iget v8, p0, Lcom/jscape/ftcl/b/a/aB;->n:I

    sub-int/2addr v7, v8

    invoke-virtual {v1, v2, v6, v7}, Ljava/io/Reader;->read([CII)I

    move-result v1

    move v2, v1

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_6

    :cond_9
    :goto_5
    if-eq v1, v4, :cond_a

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->n:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/jscape/ftcl/b/a/aB;->n:I
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    return-void

    :cond_a
    :try_start_b
    iget-object v1, p0, Lcom/jscape/ftcl/b/a/aB;->l:Ljava/io/Reader;

    invoke-virtual {v1}, Ljava/io/Reader;->close()V

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    :catch_6
    move-exception v1

    :try_start_c
    invoke-static {v1}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    throw v1
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_5

    :goto_6
    :try_start_d
    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    invoke-virtual {p0, v5}, Lcom/jscape/ftcl/b/a/aB;->c(I)V

    if-nez v0, :cond_b

    iget v0, p0, Lcom/jscape/ftcl/b/a/aB;->d:I
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_7

    if-ne v0, v4, :cond_c

    :cond_b
    iget v0, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    iput v0, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    :cond_c
    throw v1

    :catch_7
    move-exception v0

    :try_start_e
    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_8

    :catch_8
    move-exception v0

    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :catch_9
    move-exception v0

    :try_start_f
    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_a

    :catch_a
    move-exception v0

    :try_start_10
    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_b

    :catch_b
    move-exception v0

    :try_start_11
    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_c

    :catch_c
    move-exception v0

    :try_start_12
    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_d

    :catch_d
    move-exception v0

    :try_start_13
    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_e

    :catch_e
    move-exception v0

    :try_start_14
    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_f

    :catch_f
    move-exception v0

    :try_start_15
    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_10

    :catch_10
    move-exception v0

    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method protected a(C)V
    .locals 5

    invoke-static {}, Lcom/jscape/ftcl/b/a/aC;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->h:I

    const/4 v2, 0x1

    add-int/2addr v1, v2

    iput v1, p0, Lcom/jscape/ftcl/b/a/aB;->h:I

    iget-boolean v1, p0, Lcom/jscape/ftcl/b/a/aB;->k:Z

    const/4 v3, 0x0

    const/16 v4, 0xa

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    iput-boolean v3, p0, Lcom/jscape/ftcl/b/a/aB;->k:Z

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->i:I

    iput v2, p0, Lcom/jscape/ftcl/b/a/aB;->h:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/jscape/ftcl/b/a/aB;->i:I

    if-eqz v0, :cond_5

    :cond_0
    iget-boolean v1, p0, Lcom/jscape/ftcl/b/a/aB;->j:Z

    :cond_1
    if-nez v0, :cond_4

    if-eqz v1, :cond_5

    if-nez v0, :cond_3

    iput-boolean v3, p0, Lcom/jscape/ftcl/b/a/aB;->j:Z

    if-ne p1, v4, :cond_2

    iput-boolean v2, p0, Lcom/jscape/ftcl/b/a/aB;->k:Z

    if-eqz v0, :cond_5

    :cond_2
    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->i:I

    iput v2, p0, Lcom/jscape/ftcl/b/a/aB;->h:I

    add-int/lit8 v3, v1, 0x1

    :cond_3
    iput v3, p0, Lcom/jscape/ftcl/b/a/aB;->i:I

    goto :goto_0

    :cond_4
    move p1, v1

    :cond_5
    :goto_0
    const/16 v1, 0x9

    if-eq p1, v1, :cond_8

    if-eq p1, v4, :cond_7

    const/16 v1, 0xd

    if-eq p1, v1, :cond_6

    goto :goto_1

    :cond_6
    iput-boolean v2, p0, Lcom/jscape/ftcl/b/a/aB;->j:Z

    if-eqz v0, :cond_9

    :cond_7
    iput-boolean v2, p0, Lcom/jscape/ftcl/b/a/aB;->k:Z

    if-eqz v0, :cond_9

    :cond_8
    iget p1, p0, Lcom/jscape/ftcl/b/a/aB;->h:I

    sub-int/2addr p1, v2

    iput p1, p0, Lcom/jscape/ftcl/b/a/aB;->h:I

    iget v0, p0, Lcom/jscape/ftcl/b/a/aB;->p:I

    rem-int v1, p1, v0

    sub-int/2addr v0, v1

    add-int/2addr p1, v0

    iput p1, p0, Lcom/jscape/ftcl/b/a/aB;->h:I

    :cond_9
    :goto_1
    iget-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    iget v0, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->i:I

    aput v1, p1, v0

    iget-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->g:[I

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->h:I

    aput v1, p1, v0

    return-void
.end method

.method protected a(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/ftcl/b/a/aB;->p:I

    return-void
.end method

.method public a(II)V
    .locals 10

    invoke-static {}, Lcom/jscape/ftcl/b/a/aC;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    if-nez v0, :cond_1

    if-lt v2, v1, :cond_0

    sub-int/2addr v2, v1

    iget v3, p0, Lcom/jscape/ftcl/b/a/aB;->o:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    if-eqz v0, :cond_2

    :cond_0
    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    iget v3, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/jscape/ftcl/b/a/aB;->o:I

    goto :goto_0

    :cond_1
    move v3, v1

    :goto_0
    add-int/2addr v2, v3

    :cond_2
    const/4 v3, 0x0

    move v4, v3

    move v5, v4

    :cond_3
    if-ge v3, v2, :cond_5

    iget-object v5, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    iget v6, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    rem-int v7, v1, v6

    aget v8, v5, v7

    add-int/lit8 v1, v1, 0x1

    rem-int v6, v1, v6

    aget v9, v5, v6

    if-nez v0, :cond_6

    if-nez v0, :cond_6

    if-ne v8, v9, :cond_4

    aput p1, v5, v7

    iget-object v5, p0, Lcom/jscape/ftcl/b/a/aB;->g:[I

    aget v6, v5, v6

    add-int/2addr v6, v4

    aget v8, v5, v7

    sub-int/2addr v6, v8

    add-int/2addr v4, p2

    aput v4, v5, v7

    add-int/lit8 v3, v3, 0x1

    move v4, v6

    move v5, v7

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_4
    move v5, v7

    :cond_5
    :goto_1
    move v9, v2

    move v8, v3

    move v7, v5

    :cond_6
    if-ge v8, v9, :cond_a

    iget-object v5, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    add-int/lit8 v6, p1, 0x1

    aput p1, v5, v7

    iget-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->g:[I

    add-int/2addr p2, v4

    aput p2, p1, v7

    :goto_2
    add-int/lit8 p1, v3, 0x1

    if-ge v3, v2, :cond_a

    if-nez v0, :cond_b

    iget-object p2, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    iget v3, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    rem-int v7, v1, v3

    if-nez v0, :cond_8

    aget v4, p2, v7

    add-int/lit8 v1, v1, 0x1

    rem-int v3, v1, v3

    aget v3, p2, v3

    if-eq v4, v3, :cond_7

    add-int/lit8 v3, v6, 0x1

    aput v6, p2, v7

    move v6, v3

    if-eqz v0, :cond_9

    :cond_7
    iget-object p2, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    :cond_8
    aput v6, p2, v7

    if-eqz v0, :cond_9

    goto :goto_3

    :cond_9
    move v3, p1

    goto :goto_2

    :cond_a
    :goto_3
    iget-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    aget p1, p1, v7

    iput p1, p0, Lcom/jscape/ftcl/b/a/aB;->i:I

    :cond_b
    iget-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->g:[I

    aget p1, p1, v7

    iput p1, p0, Lcom/jscape/ftcl/b/a/aB;->h:I

    return-void
.end method

.method public a(Ljava/io/InputStream;)V
    .locals 2

    const/4 v0, 0x1

    const/16 v1, 0x1000

    invoke-virtual {p0, p1, v0, v0, v1}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/io/InputStream;III)V

    return-void
.end method

.method public a(Ljava/io/InputStream;II)V
    .locals 1

    const/16 v0, 0x1000

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/io/InputStream;III)V

    return-void
.end method

.method public a(Ljava/io/InputStream;III)V
    .locals 1

    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/io/Reader;III)V

    return-void
.end method

.method public a(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/16 v5, 0x1000

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/io/InputStream;Ljava/lang/String;III)V

    return-void
.end method

.method public a(Ljava/io/InputStream;Ljava/lang/String;II)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const/16 v5, 0x1000

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/io/InputStream;Ljava/lang/String;III)V

    return-void
.end method

.method public a(Ljava/io/InputStream;Ljava/lang/String;III)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    if-nez p2, :cond_0

    :try_start_0
    new-instance p2, Ljava/io/InputStreamReader;

    invoke-direct {p2, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p1, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object p2, v0

    :goto_0
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/io/Reader;III)V

    return-void
.end method

.method public a(Ljava/io/Reader;)V
    .locals 2

    const/4 v0, 0x1

    const/16 v1, 0x1000

    invoke-virtual {p0, p1, v0, v0, v1}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/io/Reader;III)V

    return-void
.end method

.method public a(Ljava/io/Reader;II)V
    .locals 1

    const/16 v0, 0x1000

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/io/Reader;III)V

    return-void
.end method

.method public a(Ljava/io/Reader;III)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->l:Ljava/io/Reader;

    invoke-static {}, Lcom/jscape/ftcl/b/a/aC;->b()Ljava/lang/String;

    move-result-object p1

    iput p2, p0, Lcom/jscape/ftcl/b/a/aB;->i:I

    add-int/lit8 p3, p3, -0x1

    iput p3, p0, Lcom/jscape/ftcl/b/a/aB;->h:I

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    if-eqz p1, :cond_0

    array-length p1, p1

    if-eq p4, p1, :cond_2

    :cond_0
    iput p4, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    iput p4, p0, Lcom/jscape/ftcl/b/a/aB;->c:I

    new-array p1, p4, [C

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    new-array p1, p4, [I

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    :cond_1
    new-array p1, p4, [I

    iput-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->g:[I

    :cond_2
    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/jscape/ftcl/b/a/aB;->j:Z

    iput-boolean p1, p0, Lcom/jscape/ftcl/b/a/aB;->k:Z

    iput p1, p0, Lcom/jscape/ftcl/b/a/aB;->n:I

    iput p1, p0, Lcom/jscape/ftcl/b/a/aB;->o:I

    iput p1, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    const/4 p1, -0x1

    iput p1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    return-void
.end method

.method protected a(Z)V
    .locals 8

    iget v0, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    add-int/lit16 v0, v0, 0x800

    new-array v0, v0, [C

    invoke-static {}, Lcom/jscape/ftcl/b/a/aC;->b()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    add-int/lit16 v3, v2, 0x800

    new-array v3, v3, [I

    add-int/lit16 v4, v2, 0x800

    new-array v4, v4, [I

    const/4 v5, 0x0

    if-nez v1, :cond_1

    if-eqz p1, :cond_0

    :try_start_0
    iget-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    iget v6, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    iget v7, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    sub-int/2addr v2, v7

    invoke-static {p1, v6, v0, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    iget v6, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    sub-int/2addr v2, v6

    iget v6, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    invoke-static {p1, v5, v0, v2, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    iget-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    iget v6, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    iget v7, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    sub-int/2addr v6, v7

    invoke-static {p1, v2, v3, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    iget v6, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    sub-int/2addr v2, v6

    iget v6, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    invoke-static {p1, v5, v3, v2, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    iget-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->g:[I

    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    iget v6, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    iget v7, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    sub-int/2addr v6, v7

    invoke-static {p1, v2, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->g:[I

    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    iget v6, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    sub-int/2addr v2, v6

    iget v6, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    invoke-static {p1, v5, v4, v2, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v4, p0, Lcom/jscape/ftcl/b/a/aB;->g:[I

    iget p1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    iget v6, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    sub-int/2addr v2, v6

    add-int/2addr p1, v2

    iput p1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    iput p1, p0, Lcom/jscape/ftcl/b/a/aB;->n:I

    if-eqz v1, :cond_2

    const/4 p1, 0x2

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    iget-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    iget v6, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    sub-int/2addr v2, v6

    invoke-static {p1, v1, v0, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    iget-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    iget v0, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    sub-int/2addr v1, v2

    invoke-static {p1, v0, v3, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    iget-object p1, p0, Lcom/jscape/ftcl/b/a/aB;->g:[I

    iget v0, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    sub-int/2addr v1, v2

    invoke-static {p1, v0, v4, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v4, p0, Lcom/jscape/ftcl/b/a/aB;->g:[I

    :cond_1
    iget p1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    iget v0, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    sub-int/2addr p1, v0

    iput p1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    iput p1, p0, Lcom/jscape/ftcl/b/a/aB;->n:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    iget p1, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    add-int/lit16 p1, p1, 0x800

    iput p1, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    iput p1, p0, Lcom/jscape/ftcl/b/a/aB;->c:I

    iput v5, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    return-void

    :catchall_1
    move-exception p1

    new-instance v0, Ljava/lang/Error;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()C
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, -0x1

    iput v0, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    invoke-virtual {p0}, Lcom/jscape/ftcl/b/a/aB;->c()C

    move-result v0

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    iput v1, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    return v0
.end method

.method protected b(I)I
    .locals 0

    iget p1, p0, Lcom/jscape/ftcl/b/a/aB;->p:I

    return p1
.end method

.method public c()C
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/ftcl/b/a/aC;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->o:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v0, :cond_3

    if-lez v1, :cond_2

    :try_start_1
    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->o:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/jscape/ftcl/b/a/aB;->o:I

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    if-nez v0, :cond_1

    :try_start_2
    iget v0, p0, Lcom/jscape/ftcl/b/a/aB;->b:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :try_start_3
    iput v0, p0, Lcom/jscape/ftcl/b/a/aB;->e:I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    :cond_0
    iget-object v0, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    aget-char v1, v0, v1

    :cond_1
    return v1

    :cond_2
    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    :cond_3
    if-nez v0, :cond_5

    :try_start_4
    iget v0, p0, Lcom/jscape/ftcl/b/a/aB;->n:I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    if-lt v1, v0, :cond_4

    :try_start_5
    invoke-virtual {p0}, Lcom/jscape/ftcl/b/a/aB;->a()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_4
    iget-object v0, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    aget-char v1, v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_5
    :goto_0
    invoke-virtual {p0, v1}, Lcom/jscape/ftcl/b/a/aB;->a(C)V

    return v1

    :catch_2
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    :catch_3
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    move-exception v0

    :try_start_9
    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    :catch_5
    move-exception v0

    invoke-static {v0}, Lcom/jscape/ftcl/b/a/aB;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method public c(I)V
    .locals 2

    invoke-static {}, Lcom/jscape/ftcl/b/a/aC;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->o:I

    add-int/2addr v1, p1

    iput v1, p0, Lcom/jscape/ftcl/b/a/aB;->o:I

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    sub-int/2addr v1, p1

    if-nez v0, :cond_0

    iput v1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    if-gez v1, :cond_1

    iget p1, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    add-int/2addr v1, p1

    :cond_0
    iput v1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    :cond_1
    return-void
.end method

.method public d()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/aB;->g:[I

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    aget v0, v0, v1

    return v0
.end method

.method public d(I)[C
    .locals 6

    invoke-static {}, Lcom/jscape/ftcl/b/a/aC;->b()Ljava/lang/String;

    move-result-object v0

    new-array v1, p1, [C

    const/4 v2, 0x0

    if-nez v0, :cond_1

    iget v3, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    add-int/lit8 v4, v3, 0x1

    if-lt v4, p1, :cond_0

    iget-object v4, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    sub-int/2addr v3, p1

    add-int/lit8 v3, v3, 0x1

    invoke-static {v4, v3, v1, v2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    iget v3, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    iget v4, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    sub-int v5, p1, v4

    add-int/lit8 v5, v5, -0x1

    sub-int/2addr v3, v5

    sub-int v4, p1, v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v0, v3, v1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    iget-object v0, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    iget v3, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    sub-int/2addr p1, v3

    add-int/lit8 p1, p1, -0x1

    add-int/lit8 v3, v3, 0x1

    invoke-static {v0, v2, v1, p1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    return-object v1
.end method

.method public e()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    aget v0, v0, v1

    return v0
.end method

.method public f()I
    .locals 2

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/aB;->g:[I

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    aget v0, v0, v1

    return v0
.end method

.method public g()I
    .locals 2

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    aget v0, v0, v1

    return v0
.end method

.method public h()I
    .locals 2

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/aB;->g:[I

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    aget v0, v0, v1

    return v0
.end method

.method public i()I
    .locals 2

    iget-object v0, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    aget v0, v0, v1

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 5

    iget v0, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    iget v1, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    if-lt v0, v1, :cond_0

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    iget v2, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    iget v3, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    sub-int/2addr v3, v2

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    iget v3, p0, Lcom/jscape/ftcl/b/a/aB;->d:I

    iget v4, p0, Lcom/jscape/ftcl/b/a/aB;->b:I

    sub-int/2addr v4, v3

    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    const/4 v3, 0x0

    iget v4, p0, Lcom/jscape/ftcl/b/a/aB;->e:I

    add-int/lit8 v4, v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/ftcl/b/a/aB;->m:[C

    iput-object v0, p0, Lcom/jscape/ftcl/b/a/aB;->f:[I

    iput-object v0, p0, Lcom/jscape/ftcl/b/a/aB;->g:[I

    return-void
.end method
