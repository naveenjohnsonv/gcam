.class public Lcom/jscape/ftcl/c;
.super Ljava/lang/Exception;


# static fields
.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/ftcl/c;->c()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x41

    invoke-static {v0}, Lcom/jscape/ftcl/c;->b(I)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static b()I
    .locals 1

    sget v0, Lcom/jscape/ftcl/c;->b:I

    return v0
.end method

.method public static b(I)V
    .locals 0

    sput p0, Lcom/jscape/ftcl/c;->b:I

    return-void
.end method

.method public static c()I
    .locals 1

    invoke-static {}, Lcom/jscape/ftcl/c;->b()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x28

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
